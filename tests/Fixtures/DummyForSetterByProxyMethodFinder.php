<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Tests\Fixtures;

class DummyForSetterByProxyMethodFinder
{
    public function set($name, $value)
    {
    }

    public function setMoreParams($name, $value, $tmp = null)
    {
    }

    public function setLessParams($name)
    {
    }

    public function setMoreReqParams($name, $value, $more)
    {
    }

    public static function setStatic($name, $value)
    {
    }

    private function setPrivate($name, $value)
    {
    }
}
