<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Exception;

abstract class AbstractPropertyAccessException extends \OutOfBoundsException implements PropertyAccessExceptionInterface
{
    /**
     * @var string
     */
    private $class;

    /**
     * @var string
     */
    private $property;

    /**
     * NotWritablePropertyException constructor.
     *
     * @param string $class
     * @param string $property
     */
    public function __construct(string $class, string $property)
    {
        $this->class = $class;
        $this->property = $property;
        parent::__construct($this->genMessage());
    }

    /**
     * @return string
     */
    abstract public function genMessage(): string;

    /**
     * @return string
     */
    public function getClass(): string
    {
        return $this->class;
    }

    /**
     * @return string
     */
    public function getProperty(): string
    {
        return $this->property;
    }
}
