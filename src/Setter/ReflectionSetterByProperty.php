<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Setter;

use ASPRO\ObjectAccess\SetterInterface;

class ReflectionSetterByProperty implements SetterInterface
{
    /**
     * @var string
     */
    private $property;

    /**
     * @var string
     */
    private $class;

    /**
     * @var null|\ReflectionProperty
     */
    private $reflection;

    /**
     * SetterByProperty constructor.
     *
     * @param string $property
     * @param string $class
     */
    public function __construct(string $property, string $class)
    {
        $this->property = $property;
        $this->class = $class;
    }

    /**
     * @return array
     */
    public function __sleep()
    {
        return [
            'property',
            'class',
        ];
    }

    /**
     * @param object|string $object
     * @param mixed         $value
     *
     * @throws \ReflectionException
     */
    public function __invoke($object, $value): void
    {
        if (null === $this->reflection) {
            $this->reflection = new \ReflectionProperty($this->class, $this->property);
            $this->reflection->setAccessible(true);
        }
        if ($this->reflection->isStatic()) {
            $this->reflection->setValue($value);
        } elseif (is_object($object)) {
            $this->reflection->setValue($object, $value);
        } else {
            throw new \RuntimeException(sprintf('Cannot set property %s::%s', $object, $this->property));
        }
    }

    /**
     * @param \ReflectionProperty $reflection
     *
     * @return ReflectionSetterByProperty
     */
    public static function fromReflection(\ReflectionProperty $reflection): self
    {
        $instance = new static(
            $reflection->getName(),
            $reflection->getDeclaringClass()->getName()
        );
        $reflection->setAccessible(true);
        $instance->reflection = $reflection;

        return $instance;
    }
}
