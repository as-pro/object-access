<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Tests\Fixtures;

use PHPUnit\Framework\TestCase;

class DummyForGetterByProxyMethod
{
    public $properties = [];
    public static $staticProperties = [];

    public function get($name)
    {
        TestCase::assertEquals(1, func_num_args());

        return $this->properties[$name] ?? null;
    }

    public static function getStatic($name)
    {
        TestCase::assertEquals(1, func_num_args());

        return self::$staticProperties[$name] ?? null;
    }

    protected function getProtected($name)
    {
        TestCase::assertEquals(1, func_num_args());

        return $this->properties[$name] ?? null;
    }

    protected static function getProtectedStatic($name)
    {
        TestCase::assertEquals(1, func_num_args());

        return self::$staticProperties[$name] ?? null;
    }

    private function getPrivate($name)
    {
        TestCase::assertEquals(1, func_num_args());

        return $this->properties[$name] ?? null;
    }

    private static function getPrivateStatic($name)
    {
        TestCase::assertEquals(1, func_num_args());

        return self::$staticProperties[$name] ?? null;
    }
}
