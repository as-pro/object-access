<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Getter;

use ASPRO\ObjectAccess\GetterInterface;
use ASPRO\ObjectAccess\Modifiers;

class ClosureGetterByProperty implements GetterInterface
{
    /**
     * @var string
     */
    private $property;

    /**
     * @var string
     */
    private $class;

    /**
     * @var Modifiers
     */
    private $modifiers;

    /**
     * @var null|\Closure
     */
    private $cb;

    /**
     * GetterByProperty constructor.
     *
     * @param string $property
     * @param string $class
     * @param int    $modifiers
     */
    public function __construct(
        string $property,
        string $class,
        int $modifiers
    ) {
        $this->property = $property;
        $this->class = $class;
        $this->modifiers = new Modifiers($modifiers);
    }

    /**
     * @return array
     */
    public function __sleep()
    {
        return [
            'property',
            'class',
            'modifiers',
        ];
    }

    /**
     * @param object|string $object
     *
     * @return mixed
     */
    public function __invoke($object)
    {
        if (null === $this->cb) {
            $property = $this->property;
            if ($this->modifiers->isStatic()) {
                $this->cb = function ($object) use ($property) {
                    return $object::${$property};
                };
            } else {
                $this->cb = function (object $object) use ($property) {
                    return $object->{$property};
                };
            }
            if (!$this->modifiers->isPublic()) {
                $this->cb = $this->cb->bindTo(null, $this->class);
            }
        }

        return $this->cb->__invoke($object);
    }

    /**
     * @param \ReflectionProperty $reflection
     *
     * @return ClosureGetterByProperty
     */
    public static function fromReflection(\ReflectionProperty $reflection): self
    {
        return new static(
            $reflection->getName(),
            $reflection->getDeclaringClass()->getName(),
            Modifiers::valueFromReflection($reflection)
        );
    }
}
