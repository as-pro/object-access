<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Tests\Finder;

use ASPRO\ObjectAccess\Finder\GetterByMethodFinder;
use ASPRO\ObjectAccess\Getter\PublicGetterByMethod;
use ASPRO\ObjectAccess\Getter\PublicStaticGetterByMethod;
use ASPRO\ObjectAccess\Getter\ReflectionGetterByMethod;
use ASPRO\ObjectAccess\Modifiers;
use ASPRO\ObjectAccess\Tests\Fixtures\DummyForGetterByMethodFinder;
use ASPRO\ObjectAccess\Tests\Fixtures\DummyForSetterByMethodFinder;
use ASPRO\ObjectAccess\Tests\Fixtures\DummyForSetterByProxyMethodFinder;
use PHPUnit\Framework\TestCase;

/**
 * @covers \ASPRO\ObjectAccess\Finder\GetterByMethodFinder
 *
 * @internal
 */
class GetterByMethodFinderTest extends TestCase
{
    public function test()
    {
        $finder = new GetterByMethodFinder('get', '', Modifiers::IS_PUBLIC);
        $accessor = $finder->findGetter(DummyForGetterByMethodFinder::class, 'public_property');
        $this->assertEquals(
            new PublicGetterByMethod('getPublicProperty'),
            $accessor
        );
    }

    public function testPrefix()
    {
        $finder = new GetterByMethodFinder('is', '', Modifiers::IS_PUBLIC);

        $accessor = $finder->findGetter(DummyForGetterByMethodFinder::class, 'public_property');
        $this->assertEquals(
            new PublicGetterByMethod('isPublicProperty'),
            $accessor
        );
    }

    public function testNotFound()
    {
        $finder = new GetterByMethodFinder('', '', Modifiers::IS_PUBLIC);
        $accessor = $finder->findGetter(DummyForGetterByMethodFinder::class, 'not_exists');
        $this->assertNull($accessor);
    }

    public function testNotFoundVoid()
    {
        $finder = new GetterByMethodFinder('get', '', Modifiers::IS_PUBLIC);
        $this->assertTrue(method_exists(DummyForGetterByMethodFinder::class, 'getVoid'));
        $accessor = $finder->findGetter(DummyForGetterByMethodFinder::class, 'void');
        $this->assertNull($accessor);
    }

    public function testNotFoundWithParameters()
    {
        $finder = new GetterByMethodFinder('set', '', Modifiers::IS_PUBLIC);
        $this->assertTrue(method_exists(DummyForSetterByMethodFinder::class, 'setPublicProperty'));

        $accessor = $finder->findGetter(DummyForSetterByMethodFinder::class, 'public_property');
        $this->assertNull($accessor);
    }

    public function testSuffix()
    {
        $finder = new GetterByMethodFinder('get_public', '', Modifiers::IS_PUBLIC);
        $accessor = $finder->findGetter(DummyForGetterByMethodFinder::class, 'property');
        $this->assertEquals(
            new PublicGetterByMethod('getPublicProperty'),
            $accessor
        );
    }

    public function testSnakeCaseSuffix()
    {
        $finder = new GetterByMethodFinder('Snake', 'Property', Modifiers::IS_PUBLIC);
        $accessor = $finder->findGetter(DummyForGetterByMethodFinder::class, 'Case');
        $this->assertEquals(
            new PublicGetterByMethod('snake_case_property'),
            $accessor
        );
    }

    public function testCase()
    {
        $finder = new GetterByMethodFinder('get', '', Modifiers::IS_PUBLIC);
        $accessor = $finder->findGetter(DummyForGetterByMethodFinder::class, 'PuBlIcPrOpErTy');
        $this->assertEquals(
            new PublicGetterByMethod('getPublicProperty'),
            $accessor
        );
    }

    public function testSnakeCase()
    {
        $finder = new GetterByMethodFinder('', '', Modifiers::IS_PUBLIC);
        $accessor = $finder->findGetter(DummyForGetterByMethodFinder::class, 'snakeCaseProperty');
        $this->assertEquals(
            new PublicGetterByMethod('snake_case_property'),
            $accessor
        );
    }

    public function testStatic()
    {
        $finder = new GetterByMethodFinder('get', '', Modifiers::IS_PUBLIC | Modifiers::IS_STATIC);

        $accessor = $finder->findGetter(DummyForGetterByMethodFinder::class, 'static_property');
        $this->assertEquals(
            new PublicStaticGetterByMethod('getStaticProperty'),
            $accessor
        );
    }

    public function testPrivate()
    {
        $finder = new GetterByMethodFinder('get', '', Modifiers::ANY_ACCESS);

        $accessor = $finder->findGetter(DummyForGetterByMethodFinder::class, 'private_property');
        $accessor = unserialize(serialize($accessor));
        $this->assertEquals(
            new ReflectionGetterByMethod('getPrivateProperty', DummyForGetterByMethodFinder::class),
            $accessor
        );
    }
}
