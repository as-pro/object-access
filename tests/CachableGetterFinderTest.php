<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Tests;

use ASPRO\ObjectAccess\CachableGetterFinder;
use ASPRO\ObjectAccess\Getter\PublicGetterByMethod;
use ASPRO\ObjectAccess\GetterFinderInterface;
use PHPStan\Testing\TestCase;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

/**
 * @internal
 * @covers \ASPRO\ObjectAccess\CachableGetterFinder
 */
class CachableGetterFinderTest extends TestCase
{
    public function test()
    {
        /** @var GetterFinderInterface|MockObject $finder */
        $finder = $this->getMockBuilder(GetterFinderInterface::class)
            ->setMethods(['findGetter'])
            ->getMock()
            ;

        $args = ['class', 'property'];
        $getter = new PublicGetterByMethod('test');

        $finder->expects($this->once())
            ->method('findGetter')
            ->with(...$args)
            ->willReturn($getter)
            ;

        $cache = new FilesystemAdapter();
        $cache->clear();

        $cachable = new CachableGetterFinder($finder, $cache);

        $result = $cachable->findGetter(...$args);
        $this->assertEquals($getter, $result);

        $result = $cachable->findGetter(...$args);
        $this->assertEquals($getter, $result);

        $cachable->reset();
        $result = $cachable->findGetter(...$args);
        $this->assertEquals($getter, $result);

        $cache->clear();
    }
}
