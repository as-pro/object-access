<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Setter;

use ASPRO\ObjectAccess\SetterInterface;

class PublicSetterByProxyMethod implements SetterInterface
{
    /**
     * @var string
     */
    private $property;

    /**
     * @var string
     */
    private $method;

    /**
     * SetterByProxyMethod constructor.
     *
     * @param string $property
     * @param string $method
     */
    public function __construct(
        string $property,
        string $method
    ) {
        $this->property = $property;
        $this->method = $method;
    }

    /**
     * @param object $object
     * @param mixed  $value
     */
    public function __invoke($object, $value): void
    {
        $object->{$this->method}($this->property, $value);
    }

    /**
     * @param \ReflectionMethod $reflection
     * @param string            $property
     *
     * @return PublicSetterByProxyMethod
     */
    public static function fromReflection(
        \ReflectionMethod $reflection,
        string $property
    ): self {
        return new static(
            $property,
            $reflection->getName()
        );
    }
}
