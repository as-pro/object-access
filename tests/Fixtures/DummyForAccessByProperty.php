<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Tests\Fixtures;

class DummyForAccessByProperty
{
    public $foo = 'foo';
    public static $staticFoo = 'static_foo';

    public $native_name;
    public $nativeName;

    protected $protectedFoo = 'protected_foo';
    protected static $protectedStaticFoo = 'protected_static_foo';

    private $privateFoo = 'private_foo';
    private static $privateStaticFoo = 'private_static_foo';
}
