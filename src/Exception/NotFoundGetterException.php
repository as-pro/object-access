<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Exception;

class NotFoundGetterException extends AbstractPropertyAccessException
{
    /**
     * @return string
     */
    public function genMessage(): string
    {
        return sprintf('Getter not found for %s@%s', $this->getClass(), $this->getProperty());
    }
}
