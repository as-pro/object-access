<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Tests\Finder;

use ASPRO\ObjectAccess\Finder\CollectionSetterFinder;
use ASPRO\ObjectAccess\Modifiers;
use ASPRO\ObjectAccess\Setter\CollectionSetter;
use ASPRO\ObjectAccess\Setter\PublicSetterByMethod;
use ASPRO\ObjectAccess\Tests\Fixtures\DummyForCollectionSetter;
use PHPUnit\Framework\TestCase;

/**
 * @covers \ASPRO\ObjectAccess\Finder\CollectionSetterFinder
 *
 * @internal
 */
class CollectionSetterFinderTest extends TestCase
{
    public function test()
    {
        $factory = new CollectionSetterFinder('add', 'remove', Modifiers::IS_PUBLIC);

        $mutator = $factory->findSetter(DummyForCollectionSetter::class, 'items');
        $this->assertEquals(
            new CollectionSetter(
                new PublicSetterByMethod('addItem'),
                new PublicSetterByMethod('removeItem')
            ),
            $mutator
        );
    }

    public function testNotFound()
    {
        $factory = new CollectionSetterFinder('add', 'remove', Modifiers::IS_PUBLIC);
        $mutator = $factory->findSetter(DummyForCollectionSetter::class, 'elements');
        $this->assertNull($mutator);
    }
}
