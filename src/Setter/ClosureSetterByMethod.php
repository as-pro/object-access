<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Setter;

use ASPRO\ObjectAccess\Modifiers;
use ASPRO\ObjectAccess\SetterInterface;

class ClosureSetterByMethod implements SetterInterface
{
    /**
     * @var string
     */
    private $method;

    /**
     * @var string
     */
    private $class;

    /**
     * @var Modifiers
     */
    private $modifiers;

    /**
     * @var null|\Closure
     */
    private $cb;

    /**
     * SetterByMethod constructor.
     *
     * @param string $method
     * @param string $class
     * @param int    $modifiers
     */
    public function __construct(
        string $method,
        string $class,
        int $modifiers
    ) {
        $this->method = $method;
        $this->class = $class;
        $this->modifiers = new Modifiers($modifiers);
    }

    /**
     * @return array
     */
    public function __sleep()
    {
        return [
            'method',
            'class',
            'modifiers',
        ];
    }

    /**
     * @param object|string $object
     * @param mixed         $value
     */
    public function __invoke($object, $value): void
    {
        if (null === $this->cb) {
            $method = $this->method;
            if ($this->modifiers->isStatic()) {
                $this->cb = function ($object, $value) use ($method) {
                    return $object::{$method}($value);
                };
            } else {
                $this->cb = function (object $object, $value) use ($method) {
                    return $object->{$method}($value);
                };
            }
            if (!$this->modifiers->isPublic()) {
                $this->cb = $this->cb->bindTo(null, $this->class);
            }
        }
        $this->cb->__invoke($object, $value);
    }

    /**
     * @param \ReflectionMethod $reflection
     *
     * @return ClosureSetterByMethod
     */
    public static function fromReflection(\ReflectionMethod $reflection): self
    {
        return new static(
            $reflection->getName(),
            $reflection->getDeclaringClass()->getName(),
            Modifiers::valueFromReflection($reflection)
        );
    }
}
