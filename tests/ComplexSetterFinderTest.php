<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Tests;

use ASPRO\ObjectAccess\ComplexSetterFinder;
use ASPRO\ObjectAccess\Finder\SetterByMethodFinder;
use ASPRO\ObjectAccess\Modifiers;
use ASPRO\ObjectAccess\Setter\PublicSetterByMethod;
use ASPRO\ObjectAccess\Tests\Fixtures\Dummy;
use PHPUnit\Framework\TestCase;

/**
 * @covers \ASPRO\ObjectAccess\ComplexSetterFinder
 *
 * @internal
 */
class ComplexSetterFinderTest extends TestCase
{
    public function test()
    {
        $factory = new ComplexSetterFinder([
            new SetterByMethodFinder('set', '', Modifiers::IS_PUBLIC),
        ]);
        $mutator = $factory->findSetter(Dummy::class, 'publicProperty');
        $this->assertEquals(
            new PublicSetterByMethod('setPublicProperty'),
            $mutator
        );
    }

    public function testEmpty()
    {
        $factory = new ComplexSetterFinder([]);
        $mutator = $factory->findSetter(Dummy::class, 'publicProperty');
        $this->assertNull($mutator);
    }

    public function testNotFound()
    {
        $factory = new ComplexSetterFinder([
            new SetterByMethodFinder('set', '', Modifiers::IS_PUBLIC),
        ]);
        $mutator = $factory->findSetter(Dummy::class, 'not_found');
        $this->assertNull($mutator);
    }
}
