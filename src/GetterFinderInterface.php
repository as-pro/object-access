<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess;

interface GetterFinderInterface
{
    /**
     * @param string $class
     * @param string $name
     *
     * @return null|GetterInterface
     */
    public function findGetter(string $class, string $name): ?GetterInterface;
}
