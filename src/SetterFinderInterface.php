<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess;

interface SetterFinderInterface
{
    /**
     * @param string $class
     * @param string $name
     *
     * @return null|SetterInterface
     */
    public function findSetter(string $class, string $name): ?SetterInterface;
}
