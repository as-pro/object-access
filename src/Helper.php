<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess;

/**
 * @internal
 */
class Helper
{
    /**
     * @param string $value
     * @param string $delimiters
     *
     * @return string
     */
    public static function camelCase(string $value, string $delimiters = '_- '): string
    {
        return str_replace(str_split($delimiters), '', ucwords($value, $delimiters));
    }

    /**
     * @param string $value
     * @param string $delimiter
     *
     * @return string
     */
    public static function snakeCase(string $value, $delimiter = '_'): string
    {
        $replaced = preg_replace(
            [
                '/([A-Z]+)([A-Z][a-z])/u',
                '/([a-z0-9])([A-Z]+)/u',
                '/([a-z0-9])[^a-z0-9]+([a-z0-9])/i',
                '/[^a-z0-9]/i',
            ],
            [
                '\1'.$delimiter.'\2',
                '\1'.$delimiter.'\2',
                '\1'.$delimiter.'\2',
                $delimiter,
            ],
            $value
        );
        if (null === $replaced) {
            return $value;
        }

        return strtolower($replaced);
    }
}
