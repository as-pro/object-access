<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Tests\Finder;

use ASPRO\ObjectAccess\Finder\GetterByPropertyFinder;
use ASPRO\ObjectAccess\Getter\PublicGetterByProperty;
use ASPRO\ObjectAccess\Getter\PublicStaticGetterByProperty;
use ASPRO\ObjectAccess\Getter\ReflectionGetterByProperty;
use ASPRO\ObjectAccess\Modifiers;
use ASPRO\ObjectAccess\Tests\Fixtures\DummyForAccessByProperty;
use PHPUnit\Framework\TestCase;

/**
 * @covers \ASPRO\ObjectAccess\Finder\GetterByPropertyFinder
 *
 * @internal
 */
class GetterByPropertyFinderTest extends TestCase
{
    public function test()
    {
        $finder = new GetterByPropertyFinder(Modifiers::IS_PUBLIC);
        $getter = $finder->findGetter(DummyForAccessByProperty::class, 'foo');
        $this->assertEquals(
            new PublicGetterByProperty('foo'),
            $getter
        );
    }

    public function testNotFound()
    {
        $finder = new GetterByPropertyFinder(Modifiers::IS_PUBLIC);
        $getter = $finder->findGetter(DummyForAccessByProperty::class, 'not_found');
        $this->assertNull($getter);
    }

    public function testStatic()
    {
        $finder = new GetterByPropertyFinder(Modifiers::IS_PUBLIC | Modifiers::IS_STATIC);
        $getter = $finder->findGetter(DummyForAccessByProperty::class, 'static_foo');
        $this->assertEquals(
            new PublicStaticGetterByProperty('staticFoo'),
            $getter
        );
    }

    public function testPrivate()
    {
        $finder = new GetterByPropertyFinder(Modifiers::ANY_ACCESS);
        $getter = $finder->findGetter(DummyForAccessByProperty::class, 'private_foo');
        $getter = unserialize(serialize($getter));
        $this->assertEquals(
            new ReflectionGetterByProperty('privateFoo', DummyForAccessByProperty::class),
            $getter
        );
    }

    public function testPriority()
    {
        $finder = new GetterByPropertyFinder(Modifiers::IS_PUBLIC);
        $getter = $finder->findGetter(DummyForAccessByProperty::class, 'native_name');
        $this->assertEquals(
            new PublicGetterByProperty('native_name'),
            $getter
        );
    }

    public function testCase()
    {
        $finder = new GetterByPropertyFinder(Modifiers::IS_PUBLIC);
        $name = 'nativeName';
        $this->assertTrue(property_exists(DummyForAccessByProperty::class, $name));
        $getter = $finder->findGetter(DummyForAccessByProperty::class, strtoupper($name));
        $this->assertNull($getter);
    }
}
