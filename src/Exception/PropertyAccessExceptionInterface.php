<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Exception;

interface PropertyAccessExceptionInterface extends ExceptionInterface
{
    public function getProperty(): string;

    public function getClass(): string;
}
