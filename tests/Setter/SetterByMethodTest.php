<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Tests\Setter;

use ASPRO\ObjectAccess\Modifiers;
use ASPRO\ObjectAccess\Setter\ClosureSetterByMethod;
use ASPRO\ObjectAccess\Setter\PublicSetterByMethod;
use ASPRO\ObjectAccess\Setter\PublicStaticSetterByMethod;
use ASPRO\ObjectAccess\Setter\ReflectionSetterByMethod;
use ASPRO\ObjectAccess\Tests\Fixtures\DummyForSetterByMethod;
use PHPStan\Testing\TestCase;

/**
 * @internal
 * @coversNothing
 */
class SetterByMethodTest extends TestCase
{
    protected $dummy;

    protected function setUp(): void
    {
        $this->dummy = new DummyForSetterByMethod();
    }

    public function dataMethods()
    {
        return [
            ['setFoo'],
            ['setStaticFoo'],
            ['setProtectedFoo'],
            ['setPrivateFoo'],
            ['setProtectedStaticFoo'],
            ['setPrivateStaticFoo'],
        ];
    }

    /**
     * @covers \ASPRO\ObjectAccess\Setter\PublicSetterByMethod
     * @dataProvider dataMethods
     *
     * @param string $methodName
     */
    public function testPublicSetter(string $methodName)
    {
        /** @var Modifiers $modifiers */
        list($reflection, $getter, $modifiers) = $this->getInfo($methodName);

        $setter = PublicSetterByMethod::fromReflection($reflection);
        $setter = unserialize(serialize($setter));

        $this->assertEquals(
            new PublicSetterByMethod($methodName),
            $setter
        );

        $value = 'test_value';

        if ($modifiers->isPublic()) {
            $setter($this->dummy, $value);
            $this->assertEquals(
                $getter(),
                $value
            );
        } else {
            $exception = null;

            try {
                $setter($this->dummy, $value);
            } catch (\Throwable $e) {
                $exception = $e;
            }
            $this->assertNotNull($exception);
        }
    }

    /**
     * @covers \ASPRO\ObjectAccess\Setter\PublicStaticSetterByMethod
     * @dataProvider dataMethods
     *
     * @param string $methodName
     */
    public function testPublicStaticSetter(string $methodName)
    {
        /** @var Modifiers $modifiers */
        list($reflection, $getter, $modifiers) = $this->getInfo($methodName);

        $setter = PublicStaticSetterByMethod::fromReflection($reflection);
        $setter = unserialize(serialize($setter));

        $class = get_class($this->dummy);

        $this->assertEquals(
            new PublicStaticSetterByMethod($methodName),
            $setter
        );

        $value = 'test_value';

        if ($modifiers->isPublic() && $modifiers->isStatic()) {
            $setter($class, $value);
            $this->assertEquals(
                $getter(),
                $value
            );

            $value = 'static_value';
            $setter($this->dummy, $value);
            $this->assertEquals(
                $getter(),
                $value
            );
        } else {
            $exception = null;

            try {
                $setter($this->dummy, $value);
            } catch (\Throwable $e) {
                $exception = $e;
            }
            $this->assertNotNull($exception);
        }
    }

    /**
     * @covers \ASPRO\ObjectAccess\Setter\ClosureSetterByMethod
     * @dataProvider dataMethods
     *
     * @param string $methodName
     */
    public function testClosureSetter(string $methodName)
    {
        /** @var Modifiers $modifiers */
        list($reflection, $getter, $modifiers) = $this->getInfo($methodName);

        $setter = ClosureSetterByMethod::fromReflection($reflection);
        $setter = unserialize(serialize($setter));

        $class = get_class($this->dummy);

        $this->assertEquals(
            new ClosureSetterByMethod(
                $methodName,
                $class,
                $modifiers->getValue()
            ),
            $setter
        );

        $value = 'test_value';
        $setter($this->dummy, $value);
        $this->assertEquals($getter(), $value);

        if ($modifiers->isStatic()) {
            $value = 'static_test';
            $setter($class, $value);
            $this->assertEquals($getter(), $value);
        } else {
            $this->expectException(\Throwable::class);
            $setter($class, $value);
        }
    }

    /**
     * @covers \ASPRO\ObjectAccess\Setter\ReflectionSetterByMethod
     * @dataProvider dataMethods
     *
     * @param string $methodName
     */
    public function testReflectionSetter(string $methodName)
    {
        /** @var Modifiers $modifiers */
        list($reflection, $getter, $modifiers) = $this->getInfo($methodName);

        $setter = ReflectionSetterByMethod::fromReflection($reflection);
        $setter = unserialize(serialize($setter));

        $class = get_class($this->dummy);

        $this->assertEquals(
            new ReflectionSetterByMethod(
                $methodName,
                $class
            ),
            $setter
        );

        $value = 'test_value';
        $setter($this->dummy, $value);
        $this->assertEquals($getter(), $value);

        if ($modifiers->isStatic()) {
            $value = 'static_test';
            $setter($class, $value);
            $this->assertEquals($getter(), $value);
        } else {
            $this->expectException(\Throwable::class);
            $setter($class, $value);
        }
    }

    protected function getInfo(string $method): array
    {
        $reflection = new \ReflectionMethod($this->dummy, $method);
        $reflection->setAccessible(true);
        $key = $reflection->getDeclaringClass()->getName().'::'.$reflection->getName();
        if ($reflection->isStatic()) {
            $getter = function () use ($key) {
                return DummyForSetterByMethod::$staticProperties[$key] ?? null;
            };
        } else {
            $getter = function () use ($key) {
                return $this->dummy->properties[$key] ?? null;
            };
        }

        return [$reflection, $getter, Modifiers::newFromReflection($reflection)];
    }
}
