<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Tests\Getter;

use ASPRO\ObjectAccess\Getter\ClosureGetterByProperty;
use ASPRO\ObjectAccess\Getter\PublicGetterByProperty;
use ASPRO\ObjectAccess\Getter\PublicStaticGetterByProperty;
use ASPRO\ObjectAccess\Getter\ReflectionGetterByProperty;
use ASPRO\ObjectAccess\Modifiers;
use ASPRO\ObjectAccess\Tests\Fixtures\DummyForAccessByProperty;
use PHPStan\Testing\TestCase;

/**
 * @internal
 * @coversNothing
 */
class GetterByPropertyTest extends TestCase
{
    protected $dummy;

    public function setUp(): void
    {
        $this->dummy = new DummyForAccessByProperty();
    }

    public function dataProperties(): array
    {
        return [
            ['foo'],
            ['staticFoo'],
            ['protectedFoo'],
            ['protectedStaticFoo'],
            ['privateFoo'],
            ['privateStaticFoo'],
        ];
    }

    /**
     * @covers \ASPRO\ObjectAccess\Getter\PublicGetterByProperty
     * @dataProvider dataProperties
     *
     * @param string $propertyName
     */
    public function testPublicGetter(string $propertyName)
    {
        /** @var Modifiers $modifiers */
        list($property, $value, $modifiers) = $this->getPropertyInfo($this->dummy, $propertyName);

        $getter = PublicGetterByProperty::fromReflection($property);
        $getter = unserialize(serialize($getter));

        $this->assertEquals(
            new PublicGetterByProperty($propertyName),
            $getter
        );

        if ($modifiers->isPublic() && !$modifiers->isStatic()) {
            $this->assertEquals($value, $getter($this->dummy));
        } else {
            $exception = null;

            try {
                $getter($this->dummy);
            } catch (\Throwable $e) {
                $exception = $e;
            }
            $this->assertNotNull($exception);
        }
    }

    /**
     * @covers \ASPRO\ObjectAccess\Getter\PublicStaticGetterByProperty
     * @dataProvider dataProperties
     *
     * @param string $propertyName
     */
    public function testPublicStaticGetter(string $propertyName)
    {
        /** @var Modifiers $modifiers */
        list($property, $value, $modifiers) = $this->getPropertyInfo($this->dummy, $propertyName);

        $getter = PublicStaticGetterByProperty::fromReflection($property);
        $getter = unserialize(serialize($getter));

        $this->assertEquals(
            new PublicStaticGetterByProperty($propertyName),
            $getter
        );

        if ($modifiers->isStatic() && $modifiers->isPublic()) {
            $this->assertEquals($value, $getter($this->dummy));
        } else {
            $exception = null;

            try {
                $getter($this->dummy);
            } catch (\Throwable $e) {
                $exception = $e;
            }
            $this->assertNotNull($exception);
        }
    }

    /**
     * @covers \ASPRO\ObjectAccess\Getter\ClosureGetterByProperty
     * @dataProvider dataProperties
     *
     * @param string $propertyName
     */
    public function testClosureGetter(string $propertyName)
    {
        /** @var Modifiers $modifiers */
        list($property, $value, $modifiers) = $this->getPropertyInfo($this->dummy, $propertyName);

        $getter = ClosureGetterByProperty::fromReflection($property);
        $getter = unserialize(serialize($getter));

        $this->assertEquals(
            new ClosureGetterByProperty(
                $propertyName,
                get_class($this->dummy),
                $modifiers->getValue()
            ),
            $getter
        );

        $this->assertEquals($value, $getter($this->dummy));

        if ($modifiers->isStatic()) {
            $this->assertEquals($value, $getter(get_class($this->dummy)));
        } else {
            $this->expectException(\Throwable::class);
            $getter(get_class($this->dummy));
        }
    }

    /**
     * @covers \ASPRO\ObjectAccess\Getter\ReflectionGetterByProperty
     * @dataProvider dataProperties
     *
     * @param string $propertyName
     */
    public function testReflectionGetter(string $propertyName)
    {
        /** @var Modifiers $modifiers */
        list($property, $value, $modifiers) = $this->getPropertyInfo($this->dummy, $propertyName);

        $getter = ReflectionGetterByProperty::fromReflection($property);
        $getter = unserialize(serialize($getter));

        $this->assertEquals(
            new ReflectionGetterByProperty(
                $propertyName,
                get_class($this->dummy)
            ),
            $getter
        );

        $this->assertEquals($value, $getter($this->dummy));

        if ($modifiers->isStatic()) {
            $this->assertEquals($value, $getter(get_class($this->dummy)));
        } else {
            $this->expectException(\Throwable::class);
            $getter(get_class($this->dummy));
        }
    }

    protected function getPropertyInfo(object $object, string $propertyName): array
    {
        $property = new \ReflectionProperty($object, $propertyName);
        $property->setAccessible(true);
        $value = $property->isStatic() ? $property->getValue() : $property->getValue($object);
        $modifiers = Modifiers::newFromReflection($property);

        return [$property, $value, $modifiers];
    }
}
