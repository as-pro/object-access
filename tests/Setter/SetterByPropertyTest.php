<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Tests\Setter;

use ASPRO\ObjectAccess\Modifiers;
use ASPRO\ObjectAccess\Setter\ClosureSetterByProperty;
use ASPRO\ObjectAccess\Setter\PublicSetterByProperty;
use ASPRO\ObjectAccess\Setter\PublicStaticSetterByProperty;
use ASPRO\ObjectAccess\Setter\ReflectionSetterByProperty;
use ASPRO\ObjectAccess\Tests\Fixtures\DummyForAccessByProperty;
use PHPStan\Testing\TestCase;

/**
 * @internal
 * @coversNothing
 */
class SetterByPropertyTest extends TestCase
{
    protected $dummy;

    protected function setUp(): void
    {
        $this->dummy = new DummyForAccessByProperty();
    }

    public function dataProperties(): array
    {
        return [
            ['foo'],
            ['staticFoo'],
            ['protectedFoo'],
            ['protectedStaticFoo'],
            ['privateFoo'],
            ['privateStaticFoo'],
        ];
    }

    /**
     * @covers \ASPRO\ObjectAccess\Setter\PublicSetterByProperty
     * @dataProvider dataProperties
     *
     * @param string $propertyName
     */
    public function testPublicSetter(string $propertyName)
    {
        /** @var Modifiers $modifiers */
        list($reflection, $getter, $modifiers) = $this->getInfo($propertyName);

        $setter = PublicSetterByProperty::fromReflection($reflection);
        $setter = unserialize(serialize($setter));

        $this->assertEquals(
            new PublicSetterByProperty($propertyName),
            $setter
        );

        $value = 'test_value';

        if ($modifiers->isPublic() && !$modifiers->isStatic()) {
            $setter($this->dummy, $value);
            $this->assertEquals(
                $getter(),
                $value
            );
        } else {
            $exception = null;

            try {
                $setter($this->dummy, $value);
            } catch (\Throwable $e) {
                $exception = $e;
            }
            $this->assertNotNull($exception);
        }
    }

    /**
     * @covers \ASPRO\ObjectAccess\Setter\PublicStaticSetterByProperty
     * @dataProvider dataProperties
     *
     * @param string $propertyName
     */
    public function testPublicStaticSetter(string $propertyName)
    {
        /** @var Modifiers $modifiers */
        list($reflection, $getter, $modifiers) = $this->getInfo($propertyName);

        $setter = PublicStaticSetterByProperty::fromReflection($reflection);
        $setter = unserialize(serialize($setter));

        $class = get_class($this->dummy);

        $this->assertEquals(
            new PublicStaticSetterByProperty($propertyName),
            $setter
        );

        $value = 'test_value';

        if ($modifiers->isPublic() && $modifiers->isStatic()) {
            $setter($class, $value);
            $this->assertEquals(
                $getter(),
                $value
            );

            $value = 'static_value';
            $setter($this->dummy, $value);
            $this->assertEquals(
                $getter(),
                $value
            );
        } else {
            $exception = null;

            try {
                $setter($this->dummy, $value);
            } catch (\Throwable $e) {
                $exception = $e;
            }
            $this->assertNotNull($exception);
        }
    }

    /**
     * @covers \ASPRO\ObjectAccess\Setter\ClosureSetterByProperty
     * @dataProvider dataProperties
     *
     * @param string $propertyName
     */
    public function testClosureSetter(string $propertyName)
    {
        /** @var Modifiers $modifiers */
        list($reflection, $getter, $modifiers) = $this->getInfo($propertyName);

        $setter = ClosureSetterByProperty::fromReflection($reflection);
        $setter = unserialize(serialize($setter));

        $class = get_class($this->dummy);

        $this->assertEquals(
            new ClosureSetterByProperty(
                $propertyName,
                $class,
                $modifiers->getValue()
            ),
            $setter
        );

        $value = 'test_value';
        $setter($this->dummy, $value);
        $this->assertEquals($getter(), $value);

        if ($modifiers->isStatic()) {
            $value = 'static_test';
            $setter($class, $value);
            $this->assertEquals($getter(), $value);
        } else {
            $this->expectException(\Throwable::class);
            $setter($class, $value);
        }
    }

    /**
     * @covers \ASPRO\ObjectAccess\Setter\ReflectionSetterByProperty
     * @dataProvider dataProperties
     *
     * @param string $propertyName
     */
    public function testReflectionSetter(string $propertyName)
    {
        /** @var Modifiers $modifiers */
        list($reflection, $getter, $modifiers) = $this->getInfo($propertyName);

        $setter = ReflectionSetterByProperty::fromReflection($reflection);
        $setter = unserialize(serialize($setter));

        $class = get_class($this->dummy);

        $this->assertEquals(
            new ReflectionSetterByProperty(
                $propertyName,
                $class
            ),
            $setter
        );

        $value = 'test_value';
        $setter($this->dummy, $value);
        $this->assertEquals($getter(), $value);

        if ($modifiers->isStatic()) {
            $value = 'static_test';
            $setter($class, $value);
            $this->assertEquals($getter(), $value);
        } else {
            $this->expectException(\Throwable::class);
            $setter($class, $value);
        }
    }

    protected function getInfo(string $propertyName): array
    {
        $reflection = new \ReflectionProperty($this->dummy, $propertyName);
        $reflection->setAccessible(true);
        if ($reflection->isStatic()) {
            $getter = function () use ($reflection) {
                return $reflection->getValue();
            };
        } else {
            $getter = function () use ($reflection) {
                return $reflection->getValue($this->dummy);
            };
        }

        return [$reflection, $getter, Modifiers::newFromReflection($reflection)];
    }
}
