# Object Access

[![Latest Stable Version](https://poser.pugx.org/as-pro/object-access/v/stable)](https://packagist.org/packages/as-pro/object-access)
[![Latest Stable Version](https://gitlab.com/as-pro/object-access/badges/master/pipeline.svg)](https://gitlab.com/as-pro/object-access/pipelines)
[![Latest Stable Version](https://gitlab.com/as-pro/object-access/badges/master/coverage.svg?job=test)](https://gitlab.com/as-pro/object-access/pipelines)
[![License](https://poser.pugx.org/as-pro/object-access/license)](https://packagist.org/packages/as-pro/object-access)

This is a library for accessing object properties via getters and setters.

## Features:
- custom finders
- any mode (public, protected, private, static)
- serializable getters and setters for permanent storage

## Installation
```bash
composer required as-pro/object-access
```

## Usage
```php
<?php

use ASPRO\ObjectAccess\Builder;
use ASPRO\ObjectAccess\Factory;
use ASPRO\ObjectAccess\Modifiers;
use ASPRO\ObjectAccess\ComplexGetterFinder;
use ASPRO\ObjectAccess\Finder\GetterByMethodFinder;
use ASPRO\ObjectAccess\Finder\GetterByPropertyFinder;
use ASPRO\ObjectAccess\Finder\GetterByProxyMethodFinder;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

$accessor = Factory::newDefaultAccessor(true); // true to enable internal getters (protected and private)

// or using builder
$accessor = Builder::newBuilder()
    ->silence(true) // don't throw exception, default: false
    ->cachable(true) // default: true
    ->withCache(new FilesystemAdapter()) // default: null
    ->withClassResolver('\get_class') // callable, default: \get_class
    ->withGetterFinder(Factory::newDefaultGetterFinder()) // default getter finder
    ->withSetterFinder(Factory::newDefaultSetterFinder()) // default setter finder
    ->build();

// or with custom finders
$accessor = Builder::newBuilder()
    ->withGetterFinder(
        new ComplexGetterFinder([
            // first try to find public getters
            new GetterByMethodFinder('get', '', Modifiers::IS_PUBLIC),
            new GetterByMethodFinder('is', '', Modifiers::IS_PUBLIC),
            new GetterByMethodFinder('has', '', Modifiers::IS_PUBLIC),
            new GetterByMethodFinder('can', '', Modifiers::IS_PUBLIC),
            new GetterByPropertyFinder(Modifiers::IS_PUBLIC),
            // then try to find private or protected getters
            new GetterByMethodFinder('get', '', Modifiers::IS_PRIVATE | Modifiers::IS_PROTECTED),
            new GetterByMethodFinder('is', '', Modifiers::IS_PRIVATE | Modifiers::IS_PROTECTED),
            new GetterByMethodFinder('has', '', Modifiers::IS_PRIVATE | Modifiers::IS_PROTECTED),
            new GetterByMethodFinder('can', '', Modifiers::IS_PRIVATE | Modifiers::IS_PROTECTED),
            new GetterByPropertyFinder(Modifiers::IS_PRIVATE | Modifiers::IS_PROTECTED),
            // then try to find magic getter
            new GetterByProxyMethodFinder('__get', Modifiers::IS_PUBLIC),
        ])
    )
    ->build()
    ;

// example class
class Dummy {
    private $foo;
    private $bar;
    
    public function getFoo()
    {
        return $this->foo;
    }
    
    public function setFoo($foo): void 
    {
        $this->foo = $foo;
    }
    
    protected function getBar()
    {
        return $this->bar;
    }
        
    protected function setBar($bar): void
    {
        $this->bar = $bar;
    }
}

$dummy = new Dummy();

// set property or throw NotFoundGetterException
$accessor->setValue($dummy, 'foo', 'test');

// get property or throw NotFoundSetterException
$foo = $accessor->getValue($dummy, 'foo');
var_dump($foo);


// check writable before set property
if ($accessor->isWritable($dummy, 'bar')) {
    $accessor->setValue($dummy, 'bar', 'test');
}

// check readable before get property
if ($accessor->isReadable($dummy, 'bar')) {
    $bar = $accessor->getValue($dummy, 'bar');
    var_dump($bar);
}

```
