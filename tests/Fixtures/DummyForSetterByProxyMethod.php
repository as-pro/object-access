<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Tests\Fixtures;

use PHPUnit\Framework\TestCase;

class DummyForSetterByProxyMethod
{
    public $properties = [];
    public static $staticProperties = [];

    public function set($name, $value)
    {
        TestCase::assertEquals(2, func_num_args());
        $this->properties[$name] = $value;
    }

    public static function setStatic($name, $value)
    {
        TestCase::assertEquals(2, func_num_args());
        self::$staticProperties[$name] = $value;
    }

    protected function setProtected($name, $value)
    {
        TestCase::assertEquals(2, func_num_args());
        $this->properties[$name] = $value;
    }

    protected static function setProtectedStatic($name, $value)
    {
        TestCase::assertEquals(2, func_num_args());
        self::$staticProperties[$name] = $value;
    }

    private function setPrivate($name, $value)
    {
        TestCase::assertEquals(2, func_num_args());
        $this->properties[$name] = $value;
    }

    private static function setPrivateStatic($name, $value)
    {
        TestCase::assertEquals(2, func_num_args());
        self::$staticProperties[$name] = $value;
    }
}
