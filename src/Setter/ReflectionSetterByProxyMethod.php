<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Setter;

use ASPRO\ObjectAccess\SetterInterface;

class ReflectionSetterByProxyMethod implements SetterInterface
{
    /**
     * @var string
     */
    private $property;

    /**
     * @var string
     */
    private $method;

    /**
     * @var string
     */
    private $class;

    /**
     * @var null|\ReflectionMethod
     */
    private $reflection;

    /**
     * SetterByProxyMethod constructor.
     *
     * @param string $property
     * @param string $method
     * @param string $class
     */
    public function __construct(
        string $property,
        string $method,
        string $class
    ) {
        $this->property = $property;
        $this->method = $method;
        $this->class = $class;
    }

    /**
     * @return array
     */
    public function __sleep()
    {
        return [
            'property',
            'method',
            'class',
        ];
    }

    /**
     * @param object|string $object
     * @param mixed         $value
     *
     * @throws \ReflectionException
     */
    public function __invoke($object, $value): void
    {
        if (null === $this->reflection) {
            $this->reflection = new \ReflectionMethod($this->class, $this->method);
            $this->reflection->setAccessible(true);
        }
        if ($this->reflection->isStatic()) {
            $this->reflection->invoke(null, $this->property, $value);
        } elseif (is_object($object)) {
            $this->reflection->invoke($object, $this->property, $value);
        } else {
            throw new \RuntimeException(sprintf('Cannot call method %s::%s', $object, $this->method));
        }
    }

    /**
     * @param \ReflectionMethod $reflection
     * @param string            $property
     *
     * @return ReflectionSetterByProxyMethod
     */
    public static function fromReflection(
        \ReflectionMethod $reflection,
        string $property
    ): self {
        $instance = new static(
            $property,
            $reflection->getName(),
            $reflection->getDeclaringClass()->getName()
        );
        $reflection->setAccessible(true);
        $instance->reflection = $reflection;

        return $instance;
    }
}
