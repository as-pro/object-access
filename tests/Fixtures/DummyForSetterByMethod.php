<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Tests\Fixtures;

use PHPUnit\Framework\TestCase;

class DummyForSetterByMethod
{
    public $properties = [];
    public static $staticProperties = [];

    public function setFoo($value)
    {
        TestCase::assertEquals(1, func_num_args());
        $this->properties[__METHOD__] = $value;
    }

    public static function setStaticFoo($value)
    {
        TestCase::assertEquals(1, func_num_args());
        self::$staticProperties[__METHOD__] = $value;
    }

    protected function setProtectedFoo($value)
    {
        TestCase::assertEquals(1, func_num_args());
        $this->properties[__METHOD__] = $value;
    }

    protected static function setProtectedStaticFoo($value)
    {
        TestCase::assertEquals(1, func_num_args());
        self::$staticProperties[__METHOD__] = $value;
    }

    private function setPrivateFoo($value)
    {
        TestCase::assertEquals(1, func_num_args());
        $this->properties[__METHOD__] = $value;
    }

    private static function setPrivateStaticFoo($value)
    {
        TestCase::assertEquals(1, func_num_args());
        self::$staticProperties[__METHOD__] = $value;
    }
}
