<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Finder;

use ASPRO\ObjectAccess\Setter\CollectionSetter;
use ASPRO\ObjectAccess\SetterFinderInterface;
use ASPRO\ObjectAccess\SetterInterface;
use Symfony\Component\Inflector\Inflector;

class CollectionSetterFinder implements SetterFinderInterface
{
    /**
     * @var SetterByMethodFinder
     */
    private $adderFinder;

    /**
     * @var SetterByMethodFinder
     */
    private $removerFinder;

    /**
     * CollectionSetterFinder constructor.
     *
     * @param string $adderPrefix
     * @param string $removerPrefix
     * @param int    $modifiers
     */
    public function __construct(string $adderPrefix, string $removerPrefix, int $modifiers)
    {
        $this->adderFinder = new SetterByMethodFinder($adderPrefix, '', $modifiers);
        $this->removerFinder = new SetterByMethodFinder($removerPrefix, '', $modifiers);
    }

    /**
     * @param string $class
     * @param string $name
     *
     * @return null|SetterInterface
     */
    public function findSetter(string $class, string $name): ?SetterInterface
    {
        $singulars = Inflector::singularize($name);
        if (!is_array($singulars)) {
            $singulars = [$singulars];
        }

        $adder = null;
        $remover = null;
        foreach ($singulars as $singular) {
            if (!$adder) {
                $adder = $this->adderFinder->findSetter($class, $singular);
            }
            if (!$remover) {
                $remover = $this->removerFinder->findSetter($class, $singular);
            }
            if ($adder && $remover) {
                break;
            }
        }

        if ($adder) {
            return new CollectionSetter($adder, $remover);
        }

        return null;
    }
}
