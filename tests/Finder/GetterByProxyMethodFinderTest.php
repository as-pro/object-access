<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Tests\Finder;

use ASPRO\ObjectAccess\Finder\GetterByMethodFinder;
use ASPRO\ObjectAccess\Finder\GetterByProxyMethodFinder;
use ASPRO\ObjectAccess\Getter\PublicGetterByProxyMethod;
use ASPRO\ObjectAccess\Getter\PublicStaticGetterByProxyMethod;
use ASPRO\ObjectAccess\Getter\ReflectionGetterByProxyMethod;
use ASPRO\ObjectAccess\Modifiers;
use ASPRO\ObjectAccess\Tests\Fixtures\DummyForGetterByProxyMethodFinder;
use ASPRO\ObjectAccess\Tests\Fixtures\DummyForSetterByMethodFinder;
use ASPRO\ObjectAccess\Tests\Fixtures\DummyForSetterByProxyMethodFinder;
use PHPUnit\Framework\TestCase;

/**
 * @covers \ASPRO\ObjectAccess\Finder\GetterByProxyMethodFinder
 *
 * @internal
 */
class GetterByProxyMethodFinderTest extends TestCase
{
    public function test()
    {
        $finder = new GetterByProxyMethodFinder('get', Modifiers::IS_PUBLIC);
        $getter = $finder->findGetter(DummyForGetterByProxyMethodFinder::class, 'virtual');
        $this->assertEquals(
            new PublicGetterByProxyMethod('virtual', 'get'),
            $getter
        );
    }

    public function testStatic()
    {
        $finder = new GetterByProxyMethodFinder('getStatic', Modifiers::IS_PUBLIC | Modifiers::IS_STATIC);
        $getter = $finder->findGetter(DummyForGetterByProxyMethodFinder::class, 'virtual');
        $this->assertEquals(
            new PublicStaticGetterByProxyMethod('virtual', 'getStatic'),
            $getter
        );
    }

    public function testPrivate()
    {
        $finder = new GetterByProxyMethodFinder('getPrivate', Modifiers::IS_PRIVATE);
        $getter = $finder->findGetter(DummyForGetterByProxyMethodFinder::class, 'virtual');
        $getter = unserialize(serialize($getter));
        $this->assertEquals(
            new ReflectionGetterByProxyMethod('virtual', 'getPrivate', DummyForGetterByProxyMethodFinder::class),
            $getter
        );
    }

    public function testNotFound()
    {
        $finder = new GetterByProxyMethodFinder('getAttribute', Modifiers::IS_PUBLIC);
        $getter = $finder->findGetter(DummyForGetterByProxyMethodFinder::class, 'virtual');
        $this->assertNull($getter);
    }

    public function testNotFoundWithParameters()
    {
        $finder = new GetterByProxyMethodFinder('set', Modifiers::IS_PUBLIC);
        $this->assertTrue(method_exists(DummyForSetterByProxyMethodFinder::class, 'set'));

        $accessor = $finder->findGetter(DummyForSetterByProxyMethodFinder::class, 'virtual');
        $this->assertNull($accessor);
    }

    public function testVoid()
    {
        $finder = new GetterByProxyMethodFinder('getVoid', Modifiers::IS_PUBLIC);
        $getter = $finder->findGetter(DummyForGetterByProxyMethodFinder::class, 'virtual');
        $this->assertNull($getter);
    }
}
