<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Tests;

use ASPRO\ObjectAccess\Factory;
use ASPRO\ObjectAccess\Getter\PublicGetterByMethod;
use ASPRO\ObjectAccess\Getter\PublicGetterByProperty;
use ASPRO\ObjectAccess\Getter\PublicGetterByProxyMethod;
use ASPRO\ObjectAccess\Getter\ReflectionGetterByMethod;
use ASPRO\ObjectAccess\Getter\ReflectionGetterByProperty;
use ASPRO\ObjectAccess\GetterFinderInterface;
use ASPRO\ObjectAccess\GetterInterface;
use ASPRO\ObjectAccess\Tests\Fixtures\DummyForAccessByProperty;
use ASPRO\ObjectAccess\Tests\Fixtures\DummyForGetterByMethod;
use PHPStan\Testing\TestCase;

/**
 * @internal
 * @coversNothing
 */
class DefaultGetterFinderTest extends TestCase
{
    /**
     * @var GetterFinderInterface
     */
    protected $finder;

    protected function setUp(): void
    {
        $this->finder = Factory::newDefaultGetterFinder(true);
    }

    public function dataForTest()
    {
        yield 'public property' => [
            new class() {
                public $foo;
            },
            'foo',
            new PublicGetterByProperty('foo'),
        ];

        yield 'public property vs public get vs public is' => [
            new class() {
                public $foo;

                public function getFoo()
                {
                }

                public function isFoo()
                {
                }
            },
            'foo',
            new PublicGetterByMethod('getFoo'),
        ];

        yield 'public property vs public is vs public has' => [
            new class() {
                public $foo;

                public function hasFoo()
                {
                }

                public function isFoo()
                {
                }
            },
            'foo',
            new PublicGetterByMethod('isFoo'),
        ];

        yield 'public property vs public can vs public has' => [
            new class() {
                public $foo;

                public function hasFoo()
                {
                }

                public function canFoo()
                {
                }
            },
            'foo',
            new PublicGetterByMethod('hasFoo'),
        ];

        yield 'public property vs public can' => [
            new class() {
                public $foo;

                public function canFoo()
                {
                }
            },
            'foo',
            new PublicGetterByMethod('canFoo'),
        ];

        yield '__get vs public property' => [
            new class() {
                public $foo;

                public function __get($name)
                {
                }
            },
            'foo',
            new PublicGetterByProperty('foo'),
        ];

        yield '__get vs protected property' => [
            self::up(new class() {
                protected $foo;

                public function __get($name)
                {
                }
            }, $class),
            'foo',
            new ReflectionGetterByProperty('foo', $class),
        ];

        yield '__get vs protected method' => [
            self::up(new class() {
                protected function getFoo()
                {
                }

                public function __get($name)
                {
                }
            }, $class),
            'foo',
            new ReflectionGetterByMethod('getFoo', $class),
        ];

        yield '__get without properties' => [
            self::up(new class() {
                public function __get($name)
                {
                }
            }, $class),
            'virtual',
            new PublicGetterByProxyMethod('virtual', '__get'),
        ];

        yield 'public property vs protected method' => [
            new class() {
                public $foo;

                protected function getFoo()
                {
                }
            },
            'foo',
            new PublicGetterByProperty('foo'),
        ];

        yield 'public property vs protected method vs public method' => [
            new class() {
                public $foo;

                protected function getFoo()
                {
                }

                public function canFoo()
                {
                }
            },
            'foo',
            new PublicGetterByMethod('canFoo'),
        ];

        // protected getters

        yield 'private property' => [
            self::up(new class() {
                private $foo;

                protected function getBar()
                {
                }
            }, $class),
            'foo',
            new ReflectionGetterByProperty('foo', $class),
        ];

        yield 'protected property vs protected get vs protected is' => [
            self::up(new class() {
                protected $foo;

                protected function getFoo()
                {
                }

                protected function isFoo()
                {
                }
            }, $class),
            'foo',
            new ReflectionGetterByMethod('getFoo', $class),
        ];

        yield 'protected property vs protected is vs protected has' => [
            self::up(new class() {
                protected $foo;

                protected function hasFoo()
                {
                }

                protected function isFoo()
                {
                }
            }, $class),
            'foo',
            new ReflectionGetterByMethod('isFoo', $class),
        ];

        yield 'protected property vs protected can vs protected has' => [
            self::up(new class() {
                protected $foo;

                protected function hasFoo()
                {
                }

                protected function canFoo()
                {
                }
            }, $class),
            'foo',
            new ReflectionGetterByMethod('hasFoo', $class),
        ];

        yield 'protected property vs protected can' => [
            self::up(new class() {
                protected $foo;

                protected function canFoo()
                {
                }
            }, $class),
            'foo',
            new ReflectionGetterByMethod('canFoo', $class),
        ];

        yield 'private parent method' => [
            self::up(new class() extends DummyForGetterByMethod {
            }, $class),
            'private_foo',
            new ReflectionGetterByMethod('getPrivateFoo', DummyForGetterByMethod::class),
        ];

        yield 'private parent property' => [
            self::up(new class() extends DummyForAccessByProperty {
            }, $class),
            'private_foo',
            new ReflectionGetterByProperty('privateFoo', DummyForAccessByProperty::class),
        ];
    }

    public static function up(object $object, &$class)
    {
        $class = get_class($object);

        return $object;
    }

    /**
     * @dataProvider dataForTest
     * @covers \ASPRO\ObjectAccess\Factory::newDefaultGetterFinder
     *
     * @param object          $object
     * @param string          $propertyName
     * @param GetterInterface $expected
     */
    public function test(object $object, string $propertyName, GetterInterface $expected)
    {
        $getter = $this->finder->findGetter(get_class($object), $propertyName);
        $getter = unserialize(serialize($getter));
        $this->assertEquals($expected, $getter);
    }
}
