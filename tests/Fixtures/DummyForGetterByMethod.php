<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Tests\Fixtures;

use PHPUnit\Framework\TestCase;

class DummyForGetterByMethod
{
    public function getFoo()
    {
        TestCase::assertEquals(0, func_num_args());

        return 'foo';
    }

    public static function getStaticFoo()
    {
        TestCase::assertEquals(0, func_num_args());

        return 'static_foo';
    }

    protected function getProtectedFoo()
    {
        TestCase::assertEquals(0, func_num_args());

        return 'protected_foo';
    }

    protected static function getProtectedStaticFoo()
    {
        TestCase::assertEquals(0, func_num_args());

        return 'protected_static_foo';
    }

    private function getPrivateFoo()
    {
        TestCase::assertEquals(0, func_num_args());

        return 'private_foo';
    }

    private static function getPrivateStaticFoo()
    {
        TestCase::assertEquals(0, func_num_args());

        return 'private_static_foo';
    }
}
