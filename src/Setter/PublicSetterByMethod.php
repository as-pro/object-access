<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Setter;

use ASPRO\ObjectAccess\SetterInterface;

class PublicSetterByMethod implements SetterInterface
{
    /**
     * @var string
     */
    private $method;

    /**
     * PublicSetterByMethod constructor.
     *
     * @param string $method
     */
    public function __construct(string $method)
    {
        $this->method = $method;
    }

    /**
     * @param object $object
     * @param mixed  $value
     */
    public function __invoke($object, $value): void
    {
        $object->{$this->method}($value);
    }

    /**
     * @param \ReflectionMethod $reflection
     *
     * @return PublicSetterByMethod
     */
    public static function fromReflection(\ReflectionMethod $reflection): self
    {
        return new static($reflection->getName());
    }
}
