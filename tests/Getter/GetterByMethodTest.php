<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Tests\Getter;

use ASPRO\ObjectAccess\Getter\ClosureGetterByMethod;
use ASPRO\ObjectAccess\Getter\PublicGetterByMethod;
use ASPRO\ObjectAccess\Getter\PublicStaticGetterByMethod;
use ASPRO\ObjectAccess\Getter\ReflectionGetterByMethod;
use ASPRO\ObjectAccess\Modifiers;
use ASPRO\ObjectAccess\Tests\Fixtures\DummyForGetterByMethod;
use PHPUnit\Framework\TestCase;

/**
 * @coversNothing
 *
 * @internal
 */
class GetterByMethodTest extends TestCase
{
    protected $dummy;

    public function setUp(): void
    {
        $this->dummy = new DummyForGetterByMethod();
    }

    public function dataMethods()
    {
        return [
            ['getFoo'],
            ['getStaticFoo'],
            ['getProtectedFoo'],
            ['getPrivateFoo'],
            ['getProtectedStaticFoo'],
            ['getPrivateStaticFoo'],
        ];
    }

    /**
     * @covers       \ASPRO\ObjectAccess\Getter\PublicGetterByMethod
     * @dataProvider dataMethods
     *
     * @param string $methodName
     */
    public function testPublicGetter(string $methodName)
    {
        /** @var Modifiers $modifiers */
        list($method, $value, $modifiers) = $this->getMethodInfo($this->dummy, $methodName);

        $getter = PublicGetterByMethod::fromReflection($method);
        $getter = unserialize(serialize($getter));

        $this->assertEquals(
            new PublicGetterByMethod($methodName),
            $getter
        );

        if ($modifiers->isPublic()) {
            $this->assertEquals($value, $getter($this->dummy));
        } else {
            $exception = null;

            try {
                $getter($this->dummy);
            } catch (\Throwable $e) {
                $exception = $e;
            }
            $this->assertNotNull($exception);
        }
    }

    /**
     * @covers       \ASPRO\ObjectAccess\Getter\PublicStaticGetterByMethod
     * @dataProvider dataMethods
     *
     * @param string $methodName
     */
    public function testPublicStaticGetter(string $methodName)
    {
        /** @var Modifiers $modifiers */
        list($method, $value, $modifiers) = $this->getMethodInfo($this->dummy, $methodName);

        $getter = PublicStaticGetterByMethod::fromReflection($method);
        $getter = unserialize(serialize($getter));

        $this->assertEquals(
            new PublicStaticGetterByMethod($methodName),
            $getter
        );
        if ($modifiers->isPublic() && $modifiers->isStatic()) {
            $this->assertEquals($value, $getter(get_class($this->dummy)));
            $this->assertEquals($value, $getter($this->dummy));
        } else {
            $exception = null;

            try {
                $getter($this->dummy);
            } catch (\Throwable $e) {
                $exception = $e;
            }
            $this->assertNotNull($exception);
        }
    }

    /**
     * @covers       \ASPRO\ObjectAccess\Getter\ClosureGetterByMethod
     * @dataProvider dataMethods
     *
     * @param string $methodName
     */
    public function testClosureGetter(string $methodName)
    {
        /** @var Modifiers $modifiers */
        list($method, $value, $modifiers) = $this->getMethodInfo($this->dummy, $methodName);

        $getter = ClosureGetterByMethod::fromReflection($method);
        $getter = unserialize(serialize($getter));

        $this->assertEquals(
            new ClosureGetterByMethod(
                $methodName,
                get_class($this->dummy),
                $modifiers->getValue()
            ),
            $getter
        );
        $this->assertEquals($value, $getter($this->dummy));

        if ($modifiers->isStatic()) {
            $this->assertEquals($value, $getter(get_class($this->dummy)));
        } else {
            $this->expectException(\Throwable::class);
            $getter(get_class($this->dummy));
        }
    }

    /**
     * @covers       \ASPRO\ObjectAccess\Getter\ReflectionGetterByMethod
     * @dataProvider dataMethods
     *
     * @param string $methodName
     */
    public function testReflectionGetter(string $methodName)
    {
        /** @var Modifiers $modifiers */
        list($method, $value, $modifiers) = $this->getMethodInfo($this->dummy, $methodName);

        $getter = ReflectionGetterByMethod::fromReflection($method);
        $getter = unserialize(serialize($getter));

        $this->assertEquals(
            new ReflectionGetterByMethod(
                $methodName,
                get_class($this->dummy)
            ),
            $getter
        );

        $this->assertEquals($value, $getter($this->dummy));

        if ($modifiers->isStatic()) {
            $this->assertEquals($value, $getter(get_class($this->dummy)));
        } else {
            $this->expectException(\Throwable::class);
            $getter(get_class($this->dummy));
        }
    }

    protected function getMethodInfo(object $object, string $methodName): array
    {
        $method = new \ReflectionMethod($object, $methodName);
        $method->setAccessible(true);
        $value = $method->isStatic() ? $method->invoke(null) : $method->invoke($object);
        $modifiers = Modifiers::newFromReflection($method);

        return [$method, $value, $modifiers];
    }
}
