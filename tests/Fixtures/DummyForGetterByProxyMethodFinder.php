<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Tests\Fixtures;

class DummyForGetterByProxyMethodFinder
{
    public function get($name)
    {
    }

    public static function getStatic($name)
    {
    }

    public function getVoid($name): void
    {
    }

    private function getPrivate($name)
    {
    }
}
