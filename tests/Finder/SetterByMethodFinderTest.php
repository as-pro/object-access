<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Tests\Finder;

use ASPRO\ObjectAccess\Finder\SetterByMethodFinder;
use ASPRO\ObjectAccess\Modifiers;
use ASPRO\ObjectAccess\Setter\PublicSetterByMethod;
use ASPRO\ObjectAccess\Setter\PublicStaticSetterByMethod;
use ASPRO\ObjectAccess\Setter\ReflectionSetterByMethod;
use ASPRO\ObjectAccess\Tests\Fixtures\DummyForGetterByMethodFinder;
use ASPRO\ObjectAccess\Tests\Fixtures\DummyForSetterByMethodFinder;
use PHPUnit\Framework\TestCase;

/**
 * @covers \ASPRO\ObjectAccess\Finder\SetterByMethodFinder
 *
 * @internal
 */
class SetterByMethodFinderTest extends TestCase
{
    public function test()
    {
        $finder = new SetterByMethodFinder('set', '', Modifiers::IS_PUBLIC);
        $getter = $finder->findSetter(DummyForSetterByMethodFinder::class, 'public_property');
        $this->assertEquals(
            new PublicSetterByMethod('setPublicProperty'),
            $getter
        );
    }

    public function testPrefixAndSuffix()
    {
        $finder = new SetterByMethodFinder('set','Property', Modifiers::IS_PUBLIC);

        $getter = $finder->findSetter(DummyForSetterByMethodFinder::class, 'public');
        $this->assertEquals(
            new PublicSetterByMethod('setPublicProperty'),
            $getter
        );
    }

    public function testNotFound()
    {
        $finder = new SetterByMethodFinder('set', '', Modifiers::IS_PUBLIC);
        $getter = $finder->findSetter(DummyForSetterByMethodFinder::class, 'not_exists');
        $this->assertNull($getter);
    }

    public function testNotFoundWithoutParameters()
    {
        $finder = new SetterByMethodFinder('', '', Modifiers::IS_PUBLIC);
        $this->assertTrue(method_exists(DummyForGetterByMethodFinder::class, 'getPublicProperty'));
        $getter = $finder->findSetter(DummyForGetterByMethodFinder::class, 'getPublicProperty');
        $this->assertNull($getter);
    }

    public function testNotFoundWithMoreParameters()
    {
        $finder = new SetterByMethodFinder('', '', Modifiers::IS_PUBLIC);
        $this->assertTrue(method_exists(DummyForSetterByMethodFinder::class, 'setPublicPropertyMore'));
        $getter = $finder->findSetter(DummyForSetterByMethodFinder::class, 'setPublicPropertyMore');
        $this->assertNull($getter);
    }

    public function testCase()
    {
        $finder = new SetterByMethodFinder('set', '', Modifiers::IS_PUBLIC);
        $getter = $finder->findSetter(DummyForSetterByMethodFinder::class, 'PuBlIcPrOpErTy');
        $this->assertEquals(
            new PublicSetterByMethod('setPublicProperty'),
            $getter
        );
    }

    public function testSnakeCase()
    {
        $finder = new SetterByMethodFinder('Snake', 'Property', Modifiers::IS_PUBLIC);
        $getter = $finder->findSetter(DummyForSetterByMethodFinder::class, 'Case');
        $this->assertEquals(
            new PublicSetterByMethod('snake_case_property'),
            $getter
        );
    }

    public function testStatic()
    {
        $finder = new SetterByMethodFinder('set', '', Modifiers::IS_PUBLIC | Modifiers::IS_STATIC);

        $getter = $finder->findSetter(DummyForSetterByMethodFinder::class, 'static_property');
        $this->assertEquals(
            new PublicStaticSetterByMethod('setStaticProperty'),
            $getter
        );
    }

    public function testPrivate()
    {
        $finder = new SetterByMethodFinder('set', '', Modifiers::IS_PRIVATE);

        $getter = $finder->findSetter(DummyForSetterByMethodFinder::class, 'private_property');
        $getter = unserialize(serialize($getter));
        $this->assertEquals(
            new ReflectionSetterByMethod('setPrivateProperty', DummyForSetterByMethodFinder::class),
            $getter
        );
    }
}
