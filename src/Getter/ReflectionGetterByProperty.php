<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Getter;

use ASPRO\ObjectAccess\GetterInterface;

class ReflectionGetterByProperty implements GetterInterface
{
    /**
     * @var string
     */
    private $property;

    /**
     * @var string
     */
    private $class;

    /**
     * @var null|\ReflectionProperty
     */
    private $reflection;

    /**
     * GetterByProperty constructor.
     *
     * @param string $property
     * @param string $class
     */
    public function __construct(
        string $property,
        string $class
    ) {
        $this->property = $property;
        $this->class = $class;
    }

    /**
     * @return array
     */
    public function __sleep()
    {
        return [
            'property',
            'class',
        ];
    }

    /**
     * @param object|string $object
     *
     * @throws \ReflectionException
     *
     * @return mixed
     */
    public function __invoke($object)
    {
        if (null === $this->reflection) {
            $this->reflection = new \ReflectionProperty($this->class, $this->property);
            $this->reflection->setAccessible(true);
        }
        if ($this->reflection->isStatic()) {
            return $this->reflection->getValue();
        }
        if (is_object($object)) {
            return $this->reflection->getValue($object);
        }

        throw new \RuntimeException(sprintf('Cannot get property %s::%s', $object, $this->property));
    }

    /**
     * @param \ReflectionProperty $reflection
     *
     * @return ReflectionGetterByProperty
     */
    public static function fromReflection(\ReflectionProperty $reflection): self
    {
        $instance = new static(
            $reflection->getName(),
            $reflection->getDeclaringClass()->getName()
        );
        $reflection->setAccessible(true);
        $instance->reflection = $reflection;

        return $instance;
    }
}
