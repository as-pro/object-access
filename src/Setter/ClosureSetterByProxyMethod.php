<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Setter;

use ASPRO\ObjectAccess\Modifiers;
use ASPRO\ObjectAccess\SetterInterface;

class ClosureSetterByProxyMethod implements SetterInterface
{
    /**
     * @var string
     */
    private $property;

    /**
     * @var string
     */
    private $method;

    /**
     * @var string
     */
    private $class;

    /**
     * @var Modifiers
     */
    private $modifiers;

    /**
     * @var null|\Closure
     */
    private $cb;

    /**
     * SetterByProxyMethod constructor.
     *
     * @param string $property
     * @param string $method
     * @param string $class
     * @param int    $modifiers
     */
    public function __construct(
        string $property,
        string $method,
        string $class,
        int $modifiers
    ) {
        $this->property = $property;
        $this->method = $method;
        $this->class = $class;
        $this->modifiers = new Modifiers($modifiers);
    }

    /**
     * @return array
     */
    public function __sleep()
    {
        return [
            'property',
            'method',
            'class',
            'modifiers',
        ];
    }

    /**
     * @param object|string $object
     * @param mixed         $value
     */
    public function __invoke($object, $value): void
    {
        if (null === $this->cb) {
            $property = $this->property;
            $method = $this->method;
            if ($this->modifiers->isStatic()) {
                $this->cb = function ($object, $value) use ($property, $method) {
                    $object::{$method}($property, $value);
                };
            } else {
                $this->cb = function (object $object, $value) use ($property, $method) {
                    $object->{$method}($property, $value);
                };
            }
            if (!$this->modifiers->isPublic()) {
                $this->cb = $this->cb->bindTo(null, $this->class);
            }
        }

        $this->cb->__invoke($object, $value);
    }

    /**
     * @param \ReflectionMethod $reflection
     * @param string            $property
     *
     * @return ClosureSetterByProxyMethod
     */
    public static function fromReflection(
        \ReflectionMethod $reflection,
        string $property
    ): self {
        return new static(
            $property,
            $reflection->getName(),
            $reflection->getDeclaringClass()->getName(),
            Modifiers::valueFromReflection($reflection)
        );
    }
}
