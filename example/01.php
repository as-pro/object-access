<?php

use ASPRO\ObjectAccess\Builder;

require __DIR__.'/../vendor/autoload.php';

$accessor = Builder::newBuilder()
                   ->silence()
                   ->build()
;

class Foo
{
    protected $bar;

    public function setBar($bar): void
    {
        $this->bar = $bar;
    }

    public function getBar()
    {
        return $this->bar;
    }
}

$foo = new Foo();
$accessor->setValue($foo, 'bar', 'example');
$bar = $accessor->getValue($foo, 'bar');

var_dump($bar);
