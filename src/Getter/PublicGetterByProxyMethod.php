<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Getter;

use ASPRO\ObjectAccess\GetterInterface;

class PublicGetterByProxyMethod implements GetterInterface
{
    /**
     * @var string
     */
    private $property;

    /**
     * @var string
     */
    private $method;

    /**
     * GetterByProxyMethod constructor.
     *
     * @param string $property
     * @param string $method
     */
    public function __construct(
        string $property,
        string $method
    ) {
        $this->property = $property;
        $this->method = $method;
    }

    /**
     * @param object $object
     *
     * @return mixed
     */
    public function __invoke($object)
    {
        return $object->{$this->method}($this->property);
    }

    /**
     * @param \ReflectionMethod $reflection
     * @param string            $property
     *
     * @return PublicGetterByProxyMethod
     */
    public static function fromReflection(\ReflectionMethod $reflection, string $property): self
    {
        return new static(
            $property,
            $reflection->getName()
        );
    }
}
