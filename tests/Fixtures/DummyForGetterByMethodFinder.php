<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Tests\Fixtures;

class DummyForGetterByMethodFinder
{
    public function snake_case_property()
    {
    }

    public function publicProperty()
    {
    }

    public function getPublicProperty()
    {
    }

    public function isPublicProperty()
    {
    }

    public function hasPublicProperty()
    {
    }

    public static function getStaticProperty()
    {
    }

    public function getVoid(): void
    {
    }

    private function getPrivateProperty()
    {
    }
}
