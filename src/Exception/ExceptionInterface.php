<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Exception;

interface ExceptionInterface extends \Throwable
{
}
