<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Getter;

use ASPRO\ObjectAccess\GetterInterface;

class PublicStaticGetterByMethod implements GetterInterface
{
    /**
     * @var string
     */
    private $method;

    /**
     * PublicStaticGetterByMethod constructor.
     *
     * @param string $method
     */
    public function __construct(string $method)
    {
        $this->method = $method;
    }

    /**
     * @param object|string $object
     *
     * @return mixed
     */
    public function __invoke($object)
    {
        return $object::{$this->method}();
    }

    /**
     * @param \ReflectionMethod $reflection
     *
     * @return PublicStaticGetterByMethod
     */
    public static function fromReflection(\ReflectionMethod $reflection): self
    {
        return new static($reflection->getName());
    }
}
