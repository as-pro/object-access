<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Setter;

use ASPRO\ObjectAccess\GetterAwareInterface;
use ASPRO\ObjectAccess\GetterAwareTrait;
use ASPRO\ObjectAccess\GetterInterface;
use ASPRO\ObjectAccess\SetterInterface;

class CollectionSetter implements SetterInterface, GetterAwareInterface
{
    use GetterAwareTrait;

    private $adder;
    private $remover;

    /**
     * CollectionMutator constructor.
     *
     * @param SetterInterface      $adder
     * @param null|SetterInterface $remover
     * @param null|GetterInterface $getter
     */
    public function __construct(
        SetterInterface $adder,
        ?SetterInterface $remover = null,
        ?GetterInterface $getter = null
    ) {
        $this->adder = $adder;
        $this->remover = $remover;
        $this->getter = $getter;
    }

    /**
     * @param object|string $object
     * @param mixed         $value
     */
    public function __invoke($object, $value): void
    {
        $collection = $this->valueToArray($value);
        $getter = $this->getGetter();
        if ($getter) {
            $prevCollection = $getter->__invoke($object);
            $prevCollection = $this->valueToArray($prevCollection);
            if ($this->remover) {
                foreach ($prevCollection as $prevItem) {
                    if (!in_array($prevItem, $collection, true)) {
                        $this->remover->__invoke($object, $prevItem);
                    }
                }
            }
            foreach ($collection as $item) {
                if (!in_array($item, $prevCollection, true)) {
                    $this->adder->__invoke($object, $item);
                }
            }
        } else {
            foreach ($collection as $item) {
                $this->adder->__invoke($object, $item);
            }
        }
    }

    /**
     * @param mixed $value
     *
     * @return array
     */
    private function valueToArray($value): array
    {
        if (empty($value)) {
            return [];
        }
        if (!is_iterable($value)) {
            $value = [$value];
        }
        if (!is_array($value)) {
            $value = iterator_to_array($value);
        }

        return $value;
    }
}
