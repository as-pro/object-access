<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Tests;

use ASPRO\ObjectAccess\Builder;
use ASPRO\ObjectAccess\CachableGetterFinder;
use ASPRO\ObjectAccess\CachableSetterFinder;
use ASPRO\ObjectAccess\Finder\GetterByMethodFinder;
use ASPRO\ObjectAccess\Finder\SetterByMethodFinder;
use ASPRO\ObjectAccess\Modifiers;
use ASPRO\ObjectAccess\Factory;
use ASPRO\ObjectAccess\ObjectAccessorInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Cache\Adapter\ArrayAdapter;

/**
 * @covers \ASPRO\ObjectAccess\Builder
 * @coversDefaultClass \ASPRO\ObjectAccess\Builder
 *
 * @internal
 */
class BuilderTest extends TestCase
{
    public static function assertAccessorBuilder(Builder $builder)
    {
        $accessor = $builder->build();

        static::assertInstanceOf(
            ObjectAccessorInterface::class,
            $accessor
        );

        static::assertEquals(
            $builder->isSilence(),
            static::getValue($accessor, 'silence')
        );

        static::assertEquals(
            $builder->getClassResolver() ?? '\get_class',
            static::getValue($accessor, 'classResolver')
        );

        if ($builder->isCachable()) {
            $cache = $builder->getCache();

            static::assertEquals(
                new CachableGetterFinder(
                    $builder->getGetterFinder() ?? Factory::newDefaultGetterFinder(),
                    $cache
                ),
                static::getValue($accessor, 'getterFinder')
            );

            static::assertEquals(
                new CachableSetterFinder(
                    $builder->getSetterFinder() ?? Factory::newDefaultSetterFinder(),
                    $cache
                ),
                static::getValue($accessor, 'setterFinder')
            );
        } else {
            static::assertEquals(
                $builder->getGetterFinder() ?? Factory::newDefaultGetterFinder(),
                static::getValue($accessor, 'getterFinder')
            );

            static::assertEquals(
                $builder->getSetterFinder() ?? Factory::newDefaultSetterFinder(),
                static::getValue($accessor, 'setterFinder')
            );
        }
    }

    public static function getValue(object $object, string $property)
    {
        $getterFinder = new \ReflectionProperty($object, $property);
        $getterFinder->setAccessible(true);

        return $getterFinder->getValue($object);
    }

    /**
     * @covers ::build
     * @covers ::newBuilder
     */
    public function testBuild()
    {
        $builder = Builder::newBuilder();
        $this->assertAccessorBuilder($builder);
    }

    /**
     * @covers ::getGetterFinder
     * @covers ::getSetterFinder
     * @covers ::withGetterFinder
     * @covers ::withSetterFinder
     */
    public function testWith()
    {
        $builder = Builder::newBuilder()
                          ->cachable(false)
        ;

        $this->assertNull($builder->getGetterFinder());
        $this->assertNull($builder->getSetterFinder());

        $getterFinder = new GetterByMethodFinder('', '', Modifiers::IS_PUBLIC);
        $setterFinder = new SetterByMethodFinder('', '', Modifiers::IS_PUBLIC);

        $builder
            ->withGetterFinder($getterFinder)
            ->withSetterFinder($setterFinder)
        ;

        $this->assertTrue($getterFinder === $builder->getGetterFinder());
        $this->assertTrue($setterFinder === $builder->getSetterFinder());

        $this->assertAccessorBuilder($builder);
    }

    /**
     * @covers ::isSilence
     * @covers ::silence
     */
    public function testSilence()
    {
        $builder = Builder::newBuilder()->cachable(false);

        $this->assertFalse($builder->isSilence());
        $builder->silence(true);
        $this->assertTrue($builder->isSilence());

        $this->assertAccessorBuilder($builder);
    }

    /**
     * @covers ::cachable
     * @covers ::getCache
     * @covers ::isCachable
     * @covers ::withCache
     */
    public function testCachable()
    {
        $builder = Builder::newBuilder();

        $this->assertTrue($builder->isCachable());
        $builder->cachable(false);
        $this->assertFalse($builder->isCachable());

        $this->assertNull($builder->getCache());
        $cache = new ArrayAdapter();
        $builder->withCache($cache);
        $this->assertTrue($cache === $builder->getCache());
        $this->assertTrue($builder->isCachable());
        $builder->withCache(null);
        $this->assertNull($builder->getCache());

        $this->assertAccessorBuilder($builder);
    }

    /**
     * @covers ::getClassResolver
     * @covers ::withClassResolver
     */
    public function testClassResolver()
    {
        $builder = Builder::newBuilder()->cachable(false);

        $cb = 'get_class';
        $this->assertNull($builder->getClassResolver());
        $builder->withClassResolver($cb);
        $this->assertEquals($cb, $builder->getClassResolver());

        $this->assertAccessorBuilder($builder);
    }

    /**
     * @covers ::cachable
     * @covers ::newBuilder
     * @covers ::silence
     * @covers ::withCache
     * @covers ::withClassResolver
     * @covers ::withGetterFinder
     * @covers ::withSetterFinder
     */
    public function testFluent()
    {
        $builder = Builder::newBuilder();
        $this->assertInstanceOf(Builder::class, $builder);
        $this->assertInstanceOf(Builder::class, $builder->silence());
        $this->assertInstanceOf(Builder::class, $builder->cachable(false));
        $this->assertInstanceOf(Builder::class, $builder->withCache(new ArrayAdapter()));
        $this->assertInstanceOf(Builder::class, $builder->withGetterFinder(null));
        $this->assertInstanceOf(Builder::class, $builder->withSetterFinder(null));
        $this->assertInstanceOf(Builder::class, $builder->withClassResolver('get_class'));

        $this->assertAccessorBuilder($builder);
    }
}
