<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Tests;

use ASPRO\ObjectAccess\ComplexGetterFinder;
use ASPRO\ObjectAccess\ComplexSetterFinder;
use ASPRO\ObjectAccess\Exception\NotFoundGetterException;
use ASPRO\ObjectAccess\Exception\NotFoundSetterException;
use ASPRO\ObjectAccess\Exception\PropertyAccessExceptionInterface;
use ASPRO\ObjectAccess\Finder\CollectionSetterFinder;
use ASPRO\ObjectAccess\Finder\GetterByMethodFinder;
use ASPRO\ObjectAccess\Finder\GetterByPropertyFinder;
use ASPRO\ObjectAccess\Finder\SetterByMethodFinder;
use ASPRO\ObjectAccess\Finder\SetterByPropertyFinder;
use ASPRO\ObjectAccess\Modifiers;
use ASPRO\ObjectAccess\ObjectAccessor;
use ASPRO\ObjectAccess\Tests\Fixtures\Dummy;
use PHPUnit\Framework\TestCase;

/**
 * @covers \ASPRO\ObjectAccess\ObjectAccessor
 *
 * @internal
 */
class ObjectAccessorTest extends TestCase
{
    /**
     * @covers \ASPRO\ObjectAccess\Exception\AbstractPropertyAccessException
     * @covers \ASPRO\ObjectAccess\Exception\NotFoundGetterException
     */
    public function testNotFoundGetterException()
    {
        $objectAccessor = new ObjectAccessor(
            new ComplexGetterFinder([]),
            new ComplexSetterFinder([])
        );

        $dummy = new Dummy();

        $this->assertFalse($objectAccessor->isReadable($dummy, 'privateProperty'));

        try {
            $objectAccessor->getValue($dummy, 'privateProperty');
        } catch (PropertyAccessExceptionInterface $e) {
            $this->assertEquals(
                $e->getClass(),
                Dummy::class
            );
            $this->assertEquals(
                $e->getProperty(),
                'privateProperty'
            );
            $this->assertEquals(
                new NotFoundGetterException(Dummy::class, 'privateProperty'),
                $e
            );
        }
    }

    /**
     * @covers \ASPRO\ObjectAccess\Exception\AbstractPropertyAccessException
     * @covers \ASPRO\ObjectAccess\Exception\NotFoundSetterException
     */
    public function testNotFoundSetterException()
    {
        $objectAccessor = new ObjectAccessor(
            new ComplexGetterFinder([]),
            new ComplexSetterFinder([])
        );

        $dummy = new Dummy();

        $this->assertFalse($objectAccessor->isWritable($dummy, 'privateProperty'));

        try {
            $objectAccessor->setValue($dummy, 'privateProperty', 'test');
        } catch (PropertyAccessExceptionInterface $e) {
            $this->assertEquals(
                $e->getClass(),
                Dummy::class
            );
            $this->assertEquals(
                $e->getProperty(),
                'privateProperty'
            );
            $this->assertEquals(
                new NotFoundSetterException(Dummy::class, 'privateProperty'),
                $e
            );
        }
    }

    public function testSilent()
    {
        $objectAccessor = new ObjectAccessor(
            new ComplexGetterFinder([]),
            new ComplexSetterFinder([]),
            true
        );

        $dummy = new Dummy();

        $objectAccessor->setValue($dummy, 'not_exist', 'test');
        $result = $objectAccessor->getValue($dummy, 'not_exist');
        $this->assertNull($result);
    }

    public function testWriteAndRead()
    {
        $objectAccessor = new ObjectAccessor(
            new GetterByMethodFinder('get', '', Modifiers::IS_PRIVATE),
            new SetterByMethodFinder('set', '', Modifiers::IS_PRIVATE)
        );

        $dummy = new Dummy();

        $value = 'test'.time();
        $objectAccessor->setValue($dummy, 'privateProperty', $value);
        $result = $objectAccessor->getValue($dummy, 'privateProperty');
        $this->assertEquals($result, $value);
    }

    public function testStaticWriteAndRead()
    {
        $objectAccessor = new ObjectAccessor(
            new GetterByMethodFinder('get', '', Modifiers::IS_PUBLIC | Modifiers::IS_STATIC),
            new SetterByMethodFinder('set', '', Modifiers::IS_PUBLIC | Modifiers::IS_STATIC)
        );

        $value = 'test'.time();
        $objectAccessor->setValue(Dummy::class, 'staticProperty', $value);
        $result = $objectAccessor->getValue(Dummy::class, 'staticProperty');
        $this->assertEquals($result, $value);
    }

    public function testWriteAndReadCollection()
    {
        $objectAccessor = new ObjectAccessor(
            new GetterByMethodFinder('get', '', Modifiers::IS_PUBLIC),
            new ComplexSetterFinder([
                new SetterByMethodFinder('set', '', Modifiers::IS_PUBLIC),
                new CollectionSetterFinder('add', 'remove', Modifiers::IS_PUBLIC),
            ])
        );

        $dummy = new Dummy();
        $dummy->items = ['a', 'b'];

        $objectAccessor->setValue($dummy, 'items', ['b', 'c']);
        $result = $objectAccessor->getValue($dummy, 'items');
        $this->assertEquals($result, ['b', 'c']);
    }

    public function testClassResolverClassNotExists()
    {
        $objectAccessor = new ObjectAccessor(
            new GetterByPropertyFinder(Modifiers::IS_PUBLIC),
            new SetterByPropertyFinder(Modifiers::IS_PUBLIC),
            false,
            function ($object) {
                return get_class($object).'Modify';
            }
        );

        $dummy = new Dummy();

        $this->expectException(\Exception::class);
        $objectAccessor->getValue($dummy, 'publicProperty');
    }

    public function testClassResolver()
    {
        $objectAccessor = new ObjectAccessor(
            new GetterByMethodFinder('get', '', Modifiers::IS_PUBLIC),
            new ComplexSetterFinder([
                new SetterByMethodFinder('set', '', Modifiers::IS_PUBLIC),
                new CollectionSetterFinder('add', 'remove', Modifiers::IS_PUBLIC),
            ]),
            false,
            function ($object) {
                return get_class($object).'Modify';
            }
        );

        $dummy = new Dummy();

        try {
            $objectAccessor->getValue($dummy, 'publicProperty');
        } catch (NotFoundGetterException $e) {
            $this->assertEquals($e->getClass(), Dummy::class.'Modify');
        }
    }
}
