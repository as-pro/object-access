<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Tests\Getter;

use ASPRO\ObjectAccess\Getter\ClosureGetterByProxyMethod;
use ASPRO\ObjectAccess\Getter\PublicGetterByProxyMethod;
use ASPRO\ObjectAccess\Getter\PublicStaticGetterByProxyMethod;
use ASPRO\ObjectAccess\Getter\ReflectionGetterByProxyMethod;
use ASPRO\ObjectAccess\Modifiers;
use ASPRO\ObjectAccess\Tests\Fixtures\DummyForGetterByProxyMethod;
use PHPStan\Testing\TestCase;

/**
 * @internal
 * @coversNothing
 */
class GetterByProxyMethodTest extends TestCase
{
    protected $dummy;

    public function setUp(): void
    {
        $this->dummy = new DummyForGetterByProxyMethod();
    }

    public function dataProxyMethods(): array
    {
        return [
            ['get'],
            ['getProtected'],
            ['getPrivate'],
            ['getStatic'],
            ['getProtectedStatic'],
            ['getPrivateStatic'],
        ];
    }

    /**
     * @covers       \ASPRO\ObjectAccess\Getter\PublicGetterByProxyMethod
     * @dataProvider dataProxyMethods
     *
     * @param string $methodName
     */
    public function testPublicGetter(string $methodName)
    {
        /** @var Modifiers $modifiers */
        list($method, $propertyName, $value, $modifiers) = $this->getProxyMethodInfo($this->dummy, $methodName);

        $getter = PublicGetterByProxyMethod::fromReflection($method, $propertyName);
        $getter = unserialize(serialize($getter));

        $this->assertEquals(
            new PublicGetterByProxyMethod($propertyName, $methodName),
            $getter
        );

        if ($modifiers->isPublic()) {
            $this->assertEquals($value, $getter($this->dummy));
        } else {
            $exception = null;

            try {
                $getter($this->dummy);
            } catch (\Throwable $e) {
                $exception = $e;
            }
            $this->assertNotNull($exception);
        }
    }

    /**
     * @covers       \ASPRO\ObjectAccess\Getter\PublicStaticGetterByProxyMethod
     * @dataProvider dataProxyMethods
     *
     * @param string $methodName
     */
    public function testPublicStaticGetter(string $methodName)
    {
        /** @var Modifiers $modifiers */
        list($method, $propertyName, $value, $modifiers) = $this->getProxyMethodInfo($this->dummy, $methodName);

        $getter = PublicStaticGetterByProxyMethod::fromReflection($method, $propertyName);
        $getter = unserialize(serialize($getter));

        $this->assertEquals(
            new PublicStaticGetterByProxyMethod($propertyName, $methodName),
            $getter
        );

        if ($modifiers->isStatic() && $modifiers->isPublic()) {
            $this->assertEquals($value, $getter($this->dummy));
        } else {
            $exception = null;

            try {
                $getter($this->dummy);
            } catch (\Throwable $e) {
                $exception = $e;
            }
            $this->assertNotNull($exception);
        }
    }

    /**
     * @covers       \ASPRO\ObjectAccess\Getter\ClosureGetterByProxyMethod
     * @dataProvider dataProxyMethods
     *
     * @param string $methodName
     */
    public function testClosureGetter(string $methodName)
    {
        /** @var Modifiers $modifiers */
        list($method, $propertyName, $value, $modifiers) = $this->getProxyMethodInfo($this->dummy, $methodName);

        $getter = ClosureGetterByProxyMethod::fromReflection($method, $propertyName);
        $getter = unserialize(serialize($getter));

        $this->assertEquals(
            new ClosureGetterByProxyMethod(
                $propertyName,
                $methodName,
                get_class($this->dummy),
                $modifiers->getValue()
            ),
            $getter
        );

        $this->assertEquals($value, $getter($this->dummy));

        if ($modifiers->isStatic()) {
            $this->assertEquals($value, $getter(get_class($this->dummy)));
        } else {
            $this->expectException(\Throwable::class);
            $getter(get_class($this->dummy));
        }
    }

    /**
     * @covers       \ASPRO\ObjectAccess\Getter\ReflectionGetterByProxyMethod
     * @dataProvider dataProxyMethods
     *
     * @param string $methodName
     */
    public function testReflectionGetter(string $methodName)
    {
        /** @var Modifiers $modifiers */
        list($method, $propertyName, $value, $modifiers) = $this->getProxyMethodInfo($this->dummy, $methodName);

        $getter = ReflectionGetterByProxyMethod::fromReflection($method, $propertyName);
        $getter = unserialize(serialize($getter));

        $this->assertEquals(
            new ReflectionGetterByProxyMethod(
                $propertyName,
                $methodName,
                get_class($this->dummy)
            ),
            $getter
        );

        $this->assertEquals($value, $getter($this->dummy));

        if ($modifiers->isStatic()) {
            $this->assertEquals($value, $getter(get_class($this->dummy)));
        } else {
            $this->expectException(\Throwable::class);
            $getter(get_class($this->dummy));
        }
    }

    protected function getProxyMethodInfo(DummyForGetterByProxyMethod $object, string $methodName): array
    {
        $proxyMethod = new \ReflectionMethod($object, $methodName);
        $proxyMethod->setAccessible(true);
        $propertyName = 'test_property_name_'.microtime();
        $value = 'test_value_'.microtime();
        if ($proxyMethod->isStatic()) {
            DummyForGetterByProxyMethod::$staticProperties[$propertyName] = $value;
        } else {
            $object->properties[$propertyName] = $value;
        }
        $modifiers = Modifiers::newFromReflection($proxyMethod);

        return [$proxyMethod, $propertyName, $value, $modifiers];
    }
}
