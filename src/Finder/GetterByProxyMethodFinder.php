<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Finder;

use ASPRO\ObjectAccess\Getter\PublicGetterByProxyMethod;
use ASPRO\ObjectAccess\Getter\PublicStaticGetterByProxyMethod;
use ASPRO\ObjectAccess\Getter\ReflectionGetterByProxyMethod;
use ASPRO\ObjectAccess\GetterFinderInterface;
use ASPRO\ObjectAccess\GetterInterface;
use ASPRO\ObjectAccess\Modifiers;

class GetterByProxyMethodFinder implements GetterFinderInterface
{
    /**
     * @var string
     */
    private $proxyMethod;

    /**
     * @var Modifiers
     */
    private $modifiers;

    /**
     * GetterByProxyMethodFinder constructor.
     *
     * @param string $proxyMethod
     * @param int    $modifiers
     */
    public function __construct(string $proxyMethod, int $modifiers)
    {
        $this->proxyMethod = $proxyMethod;
        $this->modifiers = new Modifiers($modifiers);
    }

    /**
     * @param string $class
     * @param string $name
     *
     * @return null|GetterInterface
     */
    public function findGetter(string $class, string $name): ?GetterInterface
    {
        $method = $this->findMethod($class);
        if (null === $method) {
            return null;
        }

        if ($method->isPublic()) {
            if ($method->isStatic()) {
                return PublicStaticGetterByProxyMethod::fromReflection($method, $name);
            }

            return PublicGetterByProxyMethod::fromReflection($method, $name);
        }

        return ReflectionGetterByProxyMethod::fromReflection($method, $name);
    }

    /**
     * @param string $class
     *
     * @return null|\ReflectionMethod
     */
    private function findMethod(string $class): ?\ReflectionMethod
    {
        try {
            $method = new \ReflectionMethod($class, $this->proxyMethod);
            if ($this->methodIsMatch($method)) {
                return $method;
            }
        } catch (\ReflectionException $e) {
            // continue
        }

        return null;
    }

    /**
     * @param \ReflectionMethod $method
     *
     * @return bool
     */
    private function methodIsMatch(\ReflectionMethod $method): bool
    {
        if (1 !== $method->getNumberOfRequiredParameters()) {
            return false;
        }

        $returnType = $method->getReturnType();
        if (null !== $returnType && 'void' === $returnType->getName()) {
            return false;
        }

        return $this->modifiers->matchReflection($method);
    }
}
