<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Tests;

use ASPRO\ObjectAccess\Modifiers;
use ASPRO\ObjectAccess\Tests\Fixtures\DummyForAccessByProperty;
use PHPUnit\Framework\TestCase;

/**
 * @covers \ASPRO\ObjectAccess\Modifiers
 *
 * @internal
 */
class ModifiersTest extends TestCase
{
    public function test()
    {
        $modifiers = new Modifiers(0);

        $this->assertFalse($modifiers->isPublic());
        $this->assertFalse($modifiers->isProtected());
        $this->assertFalse($modifiers->isPrivate());
        $this->assertFalse($modifiers->isStatic());

        $this->assertFalse($modifiers->has(Modifiers::IS_PUBLIC));

        $modifiers->add(Modifiers::IS_PUBLIC);
        $modifiers->add(Modifiers::IS_PROTECTED);
        $modifiers->add(Modifiers::IS_PRIVATE);
        $modifiers->add(Modifiers::IS_STATIC);

        $this->assertTrue($modifiers->isPublic());
        $this->assertTrue($modifiers->isProtected());
        $this->assertTrue($modifiers->isPrivate());
        $this->assertTrue($modifiers->isStatic());

        $this->assertTrue($modifiers->has(Modifiers::IS_PUBLIC));
    }

    public function dataProperties(): array
    {
        return [
            [
                'foo',
                Modifiers::IS_PUBLIC,
            ],
            [
                'staticFoo',
                Modifiers::IS_PUBLIC | Modifiers::IS_STATIC,
            ],
            [
                'protectedFoo',
                Modifiers::IS_PROTECTED,
            ],
            [
                'protectedStaticFoo',
                Modifiers::IS_PROTECTED | Modifiers::IS_STATIC,
            ],
            [
                'privateFoo',
                Modifiers::IS_PRIVATE,
            ],
            [
                'privateStaticFoo',
                Modifiers::IS_PRIVATE | Modifiers::IS_STATIC,
            ],
        ];
    }

    /**
     * @dataProvider dataProperties
     *
     * @param string $property
     * @param int    $expected
     *
     * @throws \ReflectionException
     */
    public function testFromReflection(string $property, int $expected)
    {
        $reflection = new \ReflectionProperty(DummyForAccessByProperty::class, $property);
        $modifiers = Modifiers::newFromReflection($reflection);
        $this->assertEquals($expected, $modifiers->getValue());

        $all = new Modifiers(Modifiers::ANY_ACCESS | Modifiers::IS_STATIC);
        $this->assertTrue($all->matchReflection($reflection));
        $this->assertTrue($all->has($modifiers->getValue()));
        $this->assertFalse($modifiers->has($all->getValue()));
    }
}
