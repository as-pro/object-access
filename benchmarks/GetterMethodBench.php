<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Benchmarks;

use ASPRO\ObjectAccess\Builder;
use ASPRO\ObjectAccess\Getter\ClosureGetterByMethod;
use ASPRO\ObjectAccess\Getter\PublicGetterByMethod;
use ASPRO\ObjectAccess\Getter\ReflectionGetterByMethod;
use ASPRO\ObjectAccess\ObjectAccessorInterface;
use PhpBench\Benchmark\Metadata\Annotations\BeforeMethods;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

/**
 * @BeforeMethods({"init"})
 */
class GetterMethodBench
{
    /**
     * @var object
     */
    protected $object;

    /**
     * @var PublicGetterByMethod
     */
    protected $publicGetter;

    /**
     * @var ClosureGetterByMethod
     */
    protected $closureGetter;

    /**
     * @var ReflectionGetterByMethod
     */
    protected $reflectionGetter;

    /**
     * @var PropertyAccessorInterface
     */
    protected $propertyAccessor;

    /**
     * @var ObjectAccessorInterface
     */
    protected $objectAccessor;

    public function init()
    {
        $this->object = new class() {
            public $bar;

            public function getBar()
            {
            }

            public static function getStaticBar()
            {
            }

            protected function getFoo()
            {
            }

            protected static function getStaticFoo()
            {
            }
        };
    }

    public function benchNative()
    {
        $this->object->getBar();
    }

    public function benchPublicGetter()
    {
        if (null === $this->publicGetter) {
            $method = new \ReflectionMethod($this->object, 'getBar');
            $this->publicGetter = PublicGetterByMethod::fromReflection($method);
        }
        $this->publicGetter->__invoke($this->object);
    }

    public function benchClosureGetter()
    {
        if (null === $this->closureGetter) {
            $method = new \ReflectionMethod($this->object, 'getBar');
            $this->closureGetter = ClosureGetterByMethod::fromReflection($method);
        }
        $this->closureGetter->__invoke($this->object);
    }

    public function benchReflectionGetter()
    {
        if (null === $this->reflectionGetter) {
            $method = new \ReflectionMethod($this->object, 'getBar');
            $this->reflectionGetter = ReflectionGetterByMethod::fromReflection($method);
        }
        $this->reflectionGetter->__invoke($this->object);
    }

    public function benchPropertyAccessor()
    {
        if (null === $this->propertyAccessor) {
            $cache = new FilesystemAdapter();
            $this->propertyAccessor = PropertyAccess::createPropertyAccessorBuilder()
                ->setCacheItemPool($cache)
                ->getPropertyAccessor()
            ;
        }
        $this->propertyAccessor->getValue($this->object, 'bar');
    }

    public function benchObjectAccessor()
    {
        if (null === $this->objectAccessor) {
            $cache = new FilesystemAdapter();
            $this->objectAccessor = Builder::newBuilder()
                                           ->withCache($cache)
                                           ->build()
            ;
        }
        $this->objectAccessor->getValue($this->object, 'bar');
    }
}
