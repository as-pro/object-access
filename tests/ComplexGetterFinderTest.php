<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Tests;

use ASPRO\ObjectAccess\ComplexGetterFinder;
use ASPRO\ObjectAccess\Finder\GetterByMethodFinder;
use ASPRO\ObjectAccess\Getter\PublicGetterByMethod;
use ASPRO\ObjectAccess\Modifiers;
use ASPRO\ObjectAccess\Tests\Fixtures\Dummy;
use PHPUnit\Framework\TestCase;

/**
 * @covers \ASPRO\ObjectAccess\ComplexGetterFinder
 *
 * @internal
 */
class ComplexGetterFinderTest extends TestCase
{
    public function test()
    {
        $factory = new ComplexGetterFinder([
            new GetterByMethodFinder('get', '', Modifiers::IS_PUBLIC),
        ]);
        $accessor = $factory->findGetter(Dummy::class, 'publicProperty');
        $this->assertEquals(
            new PublicGetterByMethod('getPublicProperty'),
            $accessor
        );
    }

    public function testEmpty()
    {
        $factory = new ComplexGetterFinder([]);
        $accessor = $factory->findGetter(Dummy::class, 'publicProperty');
        $this->assertNull($accessor);
    }

    public function testNotFound()
    {
        $factory = new ComplexGetterFinder([
            new GetterByMethodFinder('get', '', Modifiers::IS_PUBLIC),
        ]);
        $accessor = $factory->findGetter(Dummy::class, 'not_found');
        $this->assertNull($accessor);
    }
}
