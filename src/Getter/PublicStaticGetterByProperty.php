<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Getter;

use ASPRO\ObjectAccess\GetterInterface;

class PublicStaticGetterByProperty implements GetterInterface
{
    /**
     * @var string
     */
    private $property;

    /**
     * PublicStaticGetterByProperty constructor.
     *
     * @param string $property
     */
    public function __construct(string $property)
    {
        $this->property = $property;
    }

    /**
     * @param object|string $object
     *
     * @return mixed
     */
    public function __invoke($object)
    {
        return $object::${$this->property};
    }

    /**
     * @param \ReflectionProperty $reflection
     *
     * @return PublicStaticGetterByProperty
     */
    public static function fromReflection(\ReflectionProperty $reflection): self
    {
        return new static($reflection->getName());
    }
}
