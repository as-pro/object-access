<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess;

class ComplexGetterFinder implements GetterFinderInterface
{
    /**
     * @var GetterFinderInterface[]|iterable
     */
    private $factories;

    /**
     * AccessorFactory constructor.
     *
     * @param GetterFinderInterface[]|iterable $factories
     */
    public function __construct(iterable $factories)
    {
        $this->factories = $factories;
    }

    /**
     * @param string $class
     * @param string $name
     *
     * @return null|GetterInterface
     */
    public function findGetter(string $class, string $name): ?GetterInterface
    {
        foreach ($this->factories as $factory) {
            $accessor = $factory->findGetter($class, $name);
            if (!is_null($accessor)) {
                return $accessor;
            }
        }

        return null;
    }
}
