<?php

namespace ASPRO\ObjectAccess\Tests\Fixtures;

use ASPRO\ObjectAccess\ReflectionTrait;

class Reflection
{
    use ReflectionTrait {
        findMethod as public;
        findProperty as public;
        getReflectionClass as public;
    }
}
