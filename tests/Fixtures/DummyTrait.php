<?php

namespace ASPRO\ObjectAccess\Tests\Fixtures;

trait DummyTrait
{
    public $publicPropertyTrait;
    protected $protectedPropertyTrait;
    private $privatePropertyTrait;

    /**
     * @param mixed $publicPropertyTrait
     *
     * @return DummyTrait
     */
    public function setPublicPropertyTrait($publicPropertyTrait)
    {
        $this->publicPropertyTrait = $publicPropertyTrait;

        return $this;
    }

    /**
     * @param mixed $protectedPropertyTrait
     *
     * @return DummyTrait
     */
    protected function setProtectedPropertyTrait($protectedPropertyTrait)
    {
        $this->protectedPropertyTrait = $protectedPropertyTrait;

        return $this;
    }

    /**
     * @return mixed
     */
    protected function getPublicPropertyTrait()
    {
        return $this->publicPropertyTrait;
    }

    /**
     * @return mixed
     */
    private function getPrivatePropertyTrait()
    {
        return $this->privatePropertyTrait;
    }

    /**
     * @param mixed $privatePropertyTrait
     *
     * @return DummyTrait
     */
    private function setPrivatePropertyTrait($privatePropertyTrait)
    {
        $this->privatePropertyTrait = $privatePropertyTrait;

        return $this;
    }

    /**
     * @return mixed
     */
    private function getProtectedPropertyTrait()
    {
        return $this->protectedPropertyTrait;
    }
}
