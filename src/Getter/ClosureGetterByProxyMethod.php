<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Getter;

use ASPRO\ObjectAccess\GetterInterface;
use ASPRO\ObjectAccess\Modifiers;

class ClosureGetterByProxyMethod implements GetterInterface
{
    /**
     * @var string
     */
    private $property;

    /**
     * @var string
     */
    private $method;

    /**
     * @var string
     */
    private $class;

    /**
     * @var Modifiers
     */
    private $modifiers;

    /**
     * @var null|\Closure
     */
    private $cb;

    /**
     * GetterByProxyMethod constructor.
     *
     * @param string $property
     * @param string $method
     * @param string $class
     * @param int    $modifiers
     */
    public function __construct(
        string $property,
        string $method,
        string $class,
        int $modifiers
    ) {
        $this->property = $property;
        $this->method = $method;
        $this->class = $class;
        $this->modifiers = new Modifiers($modifiers);
    }

    /**
     * @return array
     */
    public function __sleep()
    {
        return [
            'property',
            'method',
            'class',
            'modifiers',
        ];
    }

    /**
     * @param object|string $object
     *
     * @return mixed
     */
    public function __invoke($object)
    {
        if (null === $this->cb) {
            $property = $this->property;
            $method = $this->method;
            if ($this->modifiers->isStatic()) {
                $this->cb = function ($object) use ($property, $method) {
                    return $object::{$method}($property);
                };
            } else {
                $this->cb = function (object $object) use ($property, $method) {
                    return $object->{$method}($property);
                };
            }
            if (!$this->modifiers->isPublic()) {
                $this->cb = $this->cb->bindTo(null, $this->class);
            }
        }

        return $this->cb->__invoke($object);
    }

    /**
     * @param \ReflectionMethod $reflection
     * @param string            $property
     *
     * @return ClosureGetterByProxyMethod
     */
    public static function fromReflection(\ReflectionMethod $reflection, string $property): self
    {
        return new static(
            $property,
            $reflection->getName(),
            $reflection->getDeclaringClass()->getName(),
            Modifiers::valueFromReflection($reflection)
        );
    }
}
