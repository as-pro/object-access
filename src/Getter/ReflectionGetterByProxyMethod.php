<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Getter;

use ASPRO\ObjectAccess\GetterInterface;

class ReflectionGetterByProxyMethod implements GetterInterface
{
    /**
     * @var string
     */
    private $property;

    /**
     * @var string
     */
    private $method;

    /**
     * @var string
     */
    private $class;

    /**
     * @var null|\ReflectionMethod
     */
    private $reflection;

    /**
     * GetterByProxyMethod constructor.
     *
     * @param string $property
     * @param string $method
     * @param string $class
     */
    public function __construct(
        string $property,
        string $method,
        string $class
    ) {
        $this->property = $property;
        $this->method = $method;
        $this->class = $class;
    }

    /**
     * @return array
     */
    public function __sleep()
    {
        return [
            'property',
            'method',
            'class',
        ];
    }

    /**
     * @param object|string $object
     *
     * @throws \ReflectionException
     *
     * @return mixed
     */
    public function __invoke($object)
    {
        if (null === $this->reflection) {
            $this->reflection = new \ReflectionMethod($this->class, $this->method);
            $this->reflection->setAccessible(true);
        }
        if ($this->reflection->isStatic()) {
            return $this->reflection->invoke(null, $this->property);
        }
        if (is_object($object)) {
            return $this->reflection->invoke($object, $this->property);
        }

        throw new \RuntimeException(sprintf('Cannot call method %s::%s', $object, $this->method));
    }

    /**
     * @param \ReflectionMethod $reflection
     * @param string            $property
     *
     * @return ReflectionGetterByProxyMethod
     */
    public static function fromReflection(\ReflectionMethod $reflection, string $property): self
    {
        $instance = new static(
            $property,
            $reflection->getName(),
            $reflection->getDeclaringClass()->getName()
        );
        $reflection->setAccessible(true);
        $instance->reflection = $reflection;

        return $instance;
    }
}
