<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Getter;

use ASPRO\ObjectAccess\GetterInterface;

class PublicGetterByProperty implements GetterInterface
{
    /**
     * @var string
     */
    private $property;

    /**
     * PublicGetterByProperty constructor.
     *
     * @param string $property
     */
    public function __construct(string $property)
    {
        $this->property = $property;
    }

    /**
     * @param object $object
     *
     * @return mixed
     */
    public function __invoke($object)
    {
        return $object->{$this->property};
    }

    /**
     * @param \ReflectionProperty $reflection
     *
     * @return PublicGetterByProperty
     */
    public static function fromReflection(\ReflectionProperty $reflection): self
    {
        return new static($reflection->getName());
    }
}
