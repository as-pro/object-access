<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Finder;

use ASPRO\ObjectAccess\Modifiers;
use ASPRO\ObjectAccess\Setter\PublicSetterByProxyMethod;
use ASPRO\ObjectAccess\Setter\PublicStaticSetterByProxyMethod;
use ASPRO\ObjectAccess\Setter\ReflectionSetterByProxyMethod;
use ASPRO\ObjectAccess\SetterFinderInterface;
use ASPRO\ObjectAccess\SetterInterface;

class SetterByProxyMethodFinder implements SetterFinderInterface
{
    /**
     * @var string
     */
    private $proxyMethod;

    /**
     * @var Modifiers
     */
    private $modifiers;

    /**
     * GetterByProxyMethodFinder constructor.
     *
     * @param string $proxyMethod
     * @param int    $modifiers
     */
    public function __construct(string $proxyMethod, int $modifiers)
    {
        $this->proxyMethod = $proxyMethod;
        $this->modifiers = new Modifiers($modifiers);
    }

    /**
     * @param string $class
     * @param string $name
     *
     * @return null|SetterInterface
     */
    public function findSetter(string $class, string $name): ?SetterInterface
    {
        $method = $this->findMethod($class);
        if (null === $method) {
            return null;
        }

        if ($method->isPublic()) {
            if ($method->isStatic()) {
                return PublicStaticSetterByProxyMethod::fromReflection($method, $name);
            }

            return PublicSetterByProxyMethod::fromReflection($method, $name);
        }

        return ReflectionSetterByProxyMethod::fromReflection($method, $name);
    }

    /**
     * @param string $class
     *
     * @return null|\ReflectionMethod
     */
    private function findMethod(string $class): ?\ReflectionMethod
    {
        try {
            $method = new \ReflectionMethod($class, $this->proxyMethod);
            if ($this->methodIsMatch($method)) {
                return $method;
            }
        } catch (\ReflectionException $e) {
            // continue
        }

        return null;
    }

    /**
     * @param \ReflectionMethod $method
     *
     * @return bool
     */
    private function methodIsMatch(\ReflectionMethod $method): bool
    {
        if ($method->getNumberOfParameters() < 2) {
            return false;
        }

        $reqParams = $method->getNumberOfRequiredParameters();
        if ($reqParams < 1 || $reqParams > 2) {
            return false;
        }

        return $this->modifiers->matchReflection($method);
    }
}
