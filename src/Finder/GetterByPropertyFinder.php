<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Finder;

use ASPRO\ObjectAccess\Getter\PublicGetterByProperty;
use ASPRO\ObjectAccess\Getter\PublicStaticGetterByProperty;
use ASPRO\ObjectAccess\Getter\ReflectionGetterByProperty;
use ASPRO\ObjectAccess\GetterFinderInterface;
use ASPRO\ObjectAccess\GetterInterface;
use ASPRO\ObjectAccess\Helper;
use ASPRO\ObjectAccess\Modifiers;

class GetterByPropertyFinder implements GetterFinderInterface
{
    /**
     * @var Modifiers
     */
    private $modifiers;

    /**
     * GetterByMethodFinder constructor.
     *
     * @param int $modifiers
     */
    public function __construct(int $modifiers)
    {
        $this->modifiers = new Modifiers($modifiers);
    }

    /**
     * @param string $class
     * @param string $name
     *
     * @return null|GetterInterface
     */
    public function findGetter(string $class, string $name): ?GetterInterface
    {
        $property = $this->findProperty($class, $name);
        if (null === $property) {
            return null;
        }

        if ($property->isPublic()) {
            if ($property->isStatic()) {
                return PublicStaticGetterByProperty::fromReflection($property);
            }

            return PublicGetterByProperty::fromReflection($property);
        }

        return ReflectionGetterByProperty::fromReflection($property);
    }

    /**
     * @param string $class
     * @param string $name
     *
     * @return null|\ReflectionProperty
     */
    private function findProperty(string $class, string $name): ?\ReflectionProperty
    {
        foreach ($this->possibleNames($name) as $propertyName) {
            try {
                $reflection = new \ReflectionClass($class);
                while (false !== $reflection) {
                    if ($reflection->hasProperty($propertyName)) {
                        $property = $reflection->getProperty($propertyName);
                        if ($this->propertyIsMatch($property)) {
                            return $property;
                        }
                    }
                    $reflection = $reflection->getParentClass();
                }
            } catch (\ReflectionException $e) {
                // continue
            }
        }

        return null;
    }

    /**
     * @param string $name
     *
     * @return \Generator
     */
    private function possibleNames(string $name): \Generator
    {
        yield $name;
        yield lcfirst(Helper::camelCase($name));
        yield Helper::snakeCase($name);
    }

    /**
     * @param \ReflectionProperty $property
     *
     * @return bool
     */
    private function propertyIsMatch(\ReflectionProperty $property): bool
    {
        return $this->modifiers->matchReflection($property);
    }
}
