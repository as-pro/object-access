<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess;

final class Modifiers
{
    public const IS_PRIVATE = 0b0001;
    public const IS_PROTECTED = 0b0010;
    public const IS_PUBLIC = 0b0100;
    public const IS_STATIC = 0b1000;

    public const ANY_ACCESS = self::IS_PRIVATE | self::IS_PROTECTED | self::IS_PUBLIC;

    /**
     * @var int
     */
    private $modifiers;

    /**
     * Modifier constructor.
     *
     * @param int $modifiers
     */
    public function __construct(int $modifiers)
    {
        $this->modifiers = $modifiers;
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->modifiers;
    }

    /**
     * @param int $modifiers
     *
     * @return Modifiers
     */
    public function add(int $modifiers): self
    {
        $this->modifiers |= $modifiers;

        return $this;
    }

    /**
     * @param int $modifiers
     *
     * @return bool
     */
    public function has(int $modifiers): bool
    {
        return $modifiers === ($this->modifiers & $modifiers);
    }

    /**
     * @return bool
     */
    public function isPublic(): bool
    {
        return $this->has(self::IS_PUBLIC);
    }

    /**
     * @return bool
     */
    public function isProtected(): bool
    {
        return $this->has(self::IS_PROTECTED);
    }

    /**
     * @return bool
     */
    public function isPrivate(): bool
    {
        return $this->has(self::IS_PRIVATE);
    }

    /**
     * @return bool
     */
    public function isStatic(): bool
    {
        return $this->has(self::IS_STATIC);
    }

    /**
     * @param \Reflector $reflection
     *
     * @return bool
     */
    public function matchReflection($reflection): bool
    {
        return $this->has(static::valueFromReflection($reflection));
    }

    /**
     * @param \Reflector $reflection
     *
     * @return int
     */
    public static function valueFromReflection($reflection): int
    {
        $modifiers = 0;
        if ($reflection instanceof \ReflectionProperty || $reflection instanceof \ReflectionMethod) {
            if ($reflection->isStatic()) {
                $modifiers |= self::IS_STATIC;
            }
            if ($reflection->isPublic()) {
                $modifiers |= self::IS_PUBLIC;
            } elseif ($reflection->isProtected()) {
                $modifiers |= self::IS_PROTECTED;
            } elseif ($reflection->isPrivate()) {
                $modifiers |= self::IS_PRIVATE;
            }
        }

        return $modifiers;
    }

    /**
     * @param \Reflector $reflection
     *
     * @return Modifiers
     */
    public static function newFromReflection($reflection): self
    {
        return new static(static::valueFromReflection($reflection));
    }
}
