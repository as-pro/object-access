<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Tests;

use ASPRO\ObjectAccess\Helper;
use PHPStan\Testing\TestCase;

/**
 * @internal
 * @coversDefaultClass \ASPRO\ObjectAccess\Helper
 * @coversNothing
 */
class HelperTest extends TestCase
{
    public function providerTestCamelCase()
    {
        return [
            ['case case Case cAse', 'CaseCaseCaseCAse'],
            ['case case_Case-cAse', 'CaseCaseCaseCAse'],
        ];
    }

    /**
     * @covers ::camelCase
     * @dataProvider providerTestCamelCase
     *
     * @param string $value
     * @param string $expected
     */
    public function testCamelCase(string $value, string $expected)
    {
        $this->assertEquals(
            $expected,
            Helper::camelCase($value)
        );
    }

    public function providerTestSnakeCase()
    {
        return [
            ['CaseCaseCase', 'case_case_case'],
            ['case Case+case', 'case_case_case'],
            ['case-Case+case', 'case_case_case'],
            ['__GetValue', '__get_value'],
            ['+GetValue', '_get_value'],
            ['XMLSimpleRedactor', 'xml_simple_redactor'],
            ['SimpleXML', 'simple_xml'],
            ['SimpleXMLRedactor', 'simple_xml_redactor'],
            ['RRedactor', 'r_redactor'],
        ];
    }

    /**
     * @covers ::snakeCase
     * @dataProvider providerTestSnakeCase
     *
     * @param string $value
     * @param string $expected
     */
    public function testSnakeCase(string $value, string $expected)
    {
        $this->assertEquals(
            $expected,
            Helper::snakeCase($value)
        );
    }
}
