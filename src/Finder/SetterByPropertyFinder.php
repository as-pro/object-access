<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Finder;

use ASPRO\ObjectAccess\Helper;
use ASPRO\ObjectAccess\Modifiers;
use ASPRO\ObjectAccess\Setter\PublicSetterByProperty;
use ASPRO\ObjectAccess\Setter\PublicStaticSetterByProperty;
use ASPRO\ObjectAccess\Setter\ReflectionSetterByProperty;
use ASPRO\ObjectAccess\SetterFinderInterface;
use ASPRO\ObjectAccess\SetterInterface;

class SetterByPropertyFinder implements SetterFinderInterface
{
    /**
     * @var Modifiers
     */
    private $modifiers;

    /**
     * GetterByMethodFinder constructor.
     *
     * @param int $modifiers
     */
    public function __construct(int $modifiers)
    {
        $this->modifiers = new Modifiers($modifiers);
    }

    /**
     * @param string $class
     * @param string $name
     *
     * @return null|SetterInterface
     */
    public function findSetter(string $class, string $name): ?SetterInterface
    {
        $property = $this->findProperty($class, $name);
        if (null === $property) {
            return null;
        }

        if ($property->isPublic()) {
            if ($property->isStatic()) {
                return PublicStaticSetterByProperty::fromReflection($property);
            }

            return PublicSetterByProperty::fromReflection($property);
        }

        return ReflectionSetterByProperty::fromReflection($property);
    }

    /**
     * @param string $class
     * @param string $name
     *
     * @return null|\ReflectionProperty
     */
    private function findProperty(string $class, string $name): ?\ReflectionProperty
    {
        foreach ($this->possibleNames($name) as $propertyName) {
            try {
                $reflection = new \ReflectionClass($class);
                while (false !== $reflection) {
                    if ($reflection->hasProperty($propertyName)) {
                        $property = $reflection->getProperty($propertyName);
                        if ($this->propertyIsMatch($property)) {
                            return $property;
                        }
                    }
                    $reflection = $reflection->getParentClass();
                }
            } catch (\ReflectionException $e) {
                // continue
            }
        }

        return null;
    }

    /**
     * @param string $name
     *
     * @return \Generator
     */
    private function possibleNames(string $name): \Generator
    {
        yield $name;
        yield lcfirst(Helper::camelCase($name));
        yield Helper::snakeCase($name);
    }

    /**
     * @param \ReflectionProperty $property
     *
     * @return bool
     */
    private function propertyIsMatch(\ReflectionProperty $property): bool
    {
        return $this->modifiers->matchReflection($property);
    }
}
