<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Getter;

use ASPRO\ObjectAccess\GetterInterface;

class ReflectionGetterByMethod implements GetterInterface
{
    /**
     * @var string
     */
    private $method;

    /**
     * @var string
     */
    private $class;

    /**
     * @var null|\ReflectionMethod
     */
    private $reflection;

    /**
     * ReflectionGetterByMethod constructor.
     *
     * @param string $method
     * @param string $class
     */
    public function __construct(
        string $method,
        string $class
    ) {
        $this->method = $method;
        $this->class = $class;
    }

    public function __sleep()
    {
        return [
            'method',
            'class',
        ];
    }

    /**
     * @param object|string $object
     *
     * @throws \ReflectionException
     *
     * @return mixed
     */
    public function __invoke($object)
    {
        if (null === $this->reflection) {
            $this->reflection = new \ReflectionMethod($this->class, $this->method);
            $this->reflection->setAccessible(true);
        }
        if ($this->reflection->isStatic()) {
            return $this->reflection->invoke(null);
        }
        if (is_object($object)) {
            return $this->reflection->invoke($object);
        }

        throw new \RuntimeException(sprintf('Cannot call method %s::%s', $object, $this->method));
    }

    /**
     * @param \ReflectionMethod $reflection
     *
     * @return ReflectionGetterByMethod
     */
    public static function fromReflection(\ReflectionMethod $reflection): self
    {
        $instance = new static(
            $reflection->getName(),
            $reflection->getDeclaringClass()->getName()
        );
        $reflection->setAccessible(true);
        $instance->reflection = $reflection;

        return $instance;
    }
}
