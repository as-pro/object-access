<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Setter;

use ASPRO\ObjectAccess\SetterInterface;

class ReflectionSetterByMethod implements SetterInterface
{
    /**
     * @var string
     */
    private $method;

    /**
     * @var string
     */
    private $class;

    /**
     * @var null|\ReflectionMethod
     */
    private $reflection;

    /**
     * ReflectionSetterByMethod constructor.
     *
     * @param string $method
     * @param string $class
     */
    public function __construct(
        string $method,
        string $class
    ) {
        $this->method = $method;
        $this->class = $class;
    }

    /**
     * @return array
     */
    public function __sleep()
    {
        return [
            'method',
            'class',
        ];
    }

    /**
     * @param object|string $object
     * @param mixed         $value
     *
     * @throws \ReflectionException
     */
    public function __invoke($object, $value): void
    {
        if (null === $this->reflection) {
            $this->reflection = new \ReflectionMethod($this->class, $this->method);
            $this->reflection->setAccessible(true);
        }
        if ($this->reflection->isStatic()) {
            $this->reflection->invoke(null, $value);
        } elseif (is_object($object)) {
            $this->reflection->invoke($object, $value);
        } else {
            throw new \RuntimeException(sprintf('Cannot call method %s::%s', $object, $this->method));
        }
    }

    /**
     * @param \ReflectionMethod $reflection
     *
     * @return ReflectionSetterByMethod
     */
    public static function fromReflection(\ReflectionMethod $reflection): self
    {
        $instance = new static(
            $reflection->getName(),
            $reflection->getDeclaringClass()->getName()
        );
        $reflection->setAccessible(true);
        $instance->reflection = $reflection;

        return $instance;
    }
}
