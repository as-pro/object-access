<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess;

interface ObjectAccessorInterface
{
    /**
     * @param object|string $object
     * @param string        $name
     *
     * @return mixed
     */
    public function getValue($object, string $name);

    /**
     * @param object|string $object
     * @param string        $name
     * @param mixed         $value
     */
    public function setValue($object, string $name, $value): void;

    /**
     * @param object|string $object
     * @param string        $name
     *
     * @return bool
     */
    public function isReadable($object, string $name): bool;

    /**
     * @param object|string $object
     * @param string        $name
     *
     * @return bool
     */
    public function isWritable($object, string $name): bool;
}
