<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess;

use Psr\Cache\CacheItemPoolInterface;

class Builder
{
    /**
     * @var null|GetterFinderInterface
     */
    private $getterFinder;

    /**
     * @var null|SetterFinderInterface
     */
    private $setterFinder;

    /**
     * @var bool
     */
    private $silence = false;

    /**
     * @var null|callable
     */
    private $classResolver;

    /**
     * @var bool
     */
    private $cachable = true;

    /**
     * @var null|CacheItemPoolInterface
     */
    private $cache;

    /**
     * @return Builder
     */
    public static function newBuilder(): self
    {
        return new static();
    }

    /**
     * @return null|GetterFinderInterface
     */
    public function getGetterFinder(): ?GetterFinderInterface
    {
        return $this->getterFinder;
    }

    /**
     * @param null|GetterFinderInterface $getterFinder
     *
     * @return Builder
     */
    public function withGetterFinder(?GetterFinderInterface $getterFinder): self
    {
        $this->getterFinder = $getterFinder;

        return $this;
    }

    /**
     * @return null|SetterFinderInterface
     */
    public function getSetterFinder(): ?SetterFinderInterface
    {
        return $this->setterFinder;
    }

    /**
     * @param null|SetterFinderInterface $setterFinder
     *
     * @return Builder
     */
    public function withSetterFinder(?SetterFinderInterface $setterFinder): self
    {
        $this->setterFinder = $setterFinder;

        return $this;
    }

    /**
     * @return bool
     */
    public function isSilence(): bool
    {
        return $this->silence;
    }

    public function silence(bool $silence = true): self
    {
        $this->silence = $silence;

        return $this;
    }

    /**
     * @return bool
     */
    public function isCachable(): bool
    {
        return $this->cachable;
    }

    /**
     * @param bool $cachable
     *
     * @return Builder
     */
    public function cachable(bool $cachable = true): self
    {
        $this->cachable = $cachable;

        return $this;
    }

    /**
     * @param null|CacheItemPoolInterface $cache
     *
     * @return Builder
     */
    public function withCache(?CacheItemPoolInterface $cache): self
    {
        $this->cache = $cache;
        if (null !== $cache) {
            $this->cachable = true;
        }

        return $this;
    }

    /**
     * @return null|CacheItemPoolInterface
     */
    public function getCache(): ?CacheItemPoolInterface
    {
        return $this->cache;
    }

    /**
     * @return null|callable
     */
    public function getClassResolver(): ?callable
    {
        return $this->classResolver;
    }

    /**
     * @param null|callable $classResolver
     *
     * @return Builder
     */
    public function withClassResolver(?callable $classResolver): self
    {
        $this->classResolver = $classResolver;

        return $this;
    }

    /**
     * @return ObjectAccessorInterface
     */
    public function build(): ObjectAccessorInterface
    {
        $getterFinder = $this->getterFinder ?? Factory::newDefaultGetterFinder();
        $setterFinder = $this->setterFinder ?? Factory::newDefaultSetterFinder();
        if ($this->cachable) {
            $getterFinder = new CachableGetterFinder($getterFinder, $this->cache);
            $setterFinder = new CachableSetterFinder($setterFinder, $this->cache);
        }

        return new ObjectAccessor(
            $getterFinder,
            $setterFinder,
            $this->silence,
            $this->classResolver
        );
    }
}
