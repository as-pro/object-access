<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess;

use ASPRO\ObjectAccess\Finder\CollectionSetterFinder;
use ASPRO\ObjectAccess\Finder\GetterByMethodFinder;
use ASPRO\ObjectAccess\Finder\GetterByPropertyFinder;
use ASPRO\ObjectAccess\Finder\GetterByProxyMethodFinder;
use ASPRO\ObjectAccess\Finder\SetterByMethodFinder;
use ASPRO\ObjectAccess\Finder\SetterByPropertyFinder;
use ASPRO\ObjectAccess\Finder\SetterByProxyMethodFinder;

class Factory
{
    public static function newDefaultAccessor(bool $includingInternal = false): ObjectAccessorInterface
    {
        return static::newBuilderWithDefaultFinders($includingInternal)
            ->build()
        ;
    }

    /**
     * @return Builder
     */
    public static function newBuilder(): Builder
    {
        return Builder::newBuilder();
    }

    /**
     * @param bool $includingInternal
     *
     * @return Builder
     */
    public static function newBuilderWithDefaultFinders(bool $includingInternal = false): Builder
    {
        return static::newBuilder()
            ->withGetterFinder(static::newDefaultGetterFinder($includingInternal))
            ->withSetterFinder(static::newDefaultSetterFinder($includingInternal))
            ;
    }

    /**
     * @param bool $includingInternal
     *
     * @return GetterFinderInterface
     */
    public static function newDefaultGetterFinder(bool $includingInternal = false): GetterFinderInterface
    {
        $finders = [];

        $finders[] = new GetterByMethodFinder('get', '', Modifiers::IS_PUBLIC);
        $finders[] = new GetterByMethodFinder('is', '', Modifiers::IS_PUBLIC);
        $finders[] = new GetterByMethodFinder('has', '', Modifiers::IS_PUBLIC);
        $finders[] = new GetterByMethodFinder('can', '', Modifiers::IS_PUBLIC);

        $finders[] = new GetterByPropertyFinder(Modifiers::IS_PUBLIC);

        if ($includingInternal) {
            $modifiers = Modifiers::IS_PRIVATE | Modifiers::IS_PROTECTED;

            $finders[] = new GetterByMethodFinder('get', '', $modifiers);
            $finders[] = new GetterByMethodFinder('is', '', $modifiers);
            $finders[] = new GetterByMethodFinder('has', '', $modifiers);
            $finders[] = new GetterByMethodFinder('can', '', $modifiers);

            $finders[] = new GetterByPropertyFinder($modifiers);
        }

        $finders[] = new GetterByProxyMethodFinder('__get', Modifiers::IS_PUBLIC);

        return new ComplexGetterFinder($finders);
    }

    /**
     * @param bool $includingInternal
     *
     * @return SetterFinderInterface
     */
    public static function newDefaultSetterFinder(bool $includingInternal = false): SetterFinderInterface
    {
        $finders = [];

        $finders[] = new SetterByMethodFinder('set', '', Modifiers::IS_PUBLIC);
        $finders[] = new CollectionSetterFinder('add', 'remove', Modifiers::IS_PUBLIC);
        $finders[] = new SetterByPropertyFinder(Modifiers::IS_PUBLIC);

        if ($includingInternal) {
            $modifiers = Modifiers::IS_PRIVATE | Modifiers::IS_PROTECTED;

            $finders[] = new SetterByMethodFinder('set', '', $modifiers);
            $finders[] = new CollectionSetterFinder('add', 'remove', $modifiers);
            $finders[] = new SetterByPropertyFinder($modifiers);
        }

        $finders[] = new SetterByProxyMethodFinder('__set', Modifiers::IS_PUBLIC);

        return new ComplexSetterFinder($finders);
    }
}
