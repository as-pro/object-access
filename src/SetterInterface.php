<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess;

interface SetterInterface
{
    /**
     * @param object|string $object
     * @param mixed         $value
     */
    public function __invoke($object, $value): void;
}
