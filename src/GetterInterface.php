<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess;

interface GetterInterface
{
    /**
     * @param object|string $object
     *
     * @return mixed
     */
    public function __invoke($object);
}
