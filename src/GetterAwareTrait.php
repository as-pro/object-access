<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess;

trait GetterAwareTrait
{
    /**
     * @var null|GetterInterface
     */
    private $getter;

    /**
     * @param GetterInterface $getter
     */
    public function setGetter(GetterInterface $getter): void
    {
        $this->getter = $getter;
    }

    /**
     * @return null|GetterInterface
     */
    public function getGetter(): ?GetterInterface
    {
        return $this->getter;
    }
}
