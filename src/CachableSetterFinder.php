<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess;

use Psr\Cache\CacheItemPoolInterface;

class CachableSetterFinder implements SetterFinderInterface
{
    /**
     * @var SetterFinderInterface
     */
    private $base;

    /**
     * @var null|CacheItemPoolInterface
     */
    private $cache;

    /**
     * @var array
     */
    private $arrayCache = [];

    /**
     * CachableSetterFinder constructor.
     *
     * @param SetterFinderInterface       $base
     * @param null|CacheItemPoolInterface $cache
     */
    public function __construct(SetterFinderInterface $base, ?CacheItemPoolInterface $cache = null)
    {
        $this->base = $base;
        $this->cache = $cache;
    }

    /**
     * @param string $class
     * @param string $name
     *
     * @throws \Psr\Cache\InvalidArgumentException
     *
     * @return null|SetterInterface
     */
    public function findSetter(string $class, string $name): ?SetterInterface
    {
        $key = __CLASS__.'..'.$class.'..'.$name;
        $key = str_replace(['\\', '/', '@', '{', '}', '(', ')', ':'], '.', $key);

        if (isset($this->arrayCache[$key])) {
            $result = $this->arrayCache[$key];

            return false === $result ? null : $result;
        }

        if (null !== $this->cache) {
            $item = $this->cache->getItem($key);
            if ($item->isHit()) {
                $result = $item->get();
                $this->arrayCache[$key] = $result;

                return false === $result ? null : $result;
            }
        }

        $result = $this->base->findSetter($class, $name);

        $this->arrayCache[$key] = $result ?? false;

        if (isset($item) && null !== $this->cache) {
            $this->cache->save($item->set($result));
        }

        return $result;
    }

    public function reset(): void
    {
        $this->arrayCache = [];
    }
}
