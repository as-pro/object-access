<?php

namespace ASPRO\ObjectAccess\Tests\Fixtures;

class Dummy extends DummyParent
{
    public $publicProperty;
    public $public_property;
    public $attributes = [];
    public $items = [];
    public $enabled = false;
    public $action = false;

    public static $staticProperty;

    protected $protectedProperty;

    private $privateProperty;

    public static function getStaticProperty()
    {
        return static::$staticProperty;
    }

    public static function setStaticProperty($value)
    {
        static::$staticProperty = $value;
    }

    public function __get($name)
    {
        return $this->attributes[$name] ?? null;
    }

    public function __set($name, $value)
    {
        $this->attributes[$name] = $value;
    }

    /**
     * @return mixed
     */
    public function getPublicProperty()
    {
        return $this->publicProperty;
    }

    /**
     * @param mixed $publicProperty
     */
    public function setPublicProperty($publicProperty): void
    {
        $this->publicProperty = $publicProperty;
    }

    public function noReturnMethod(): void
    {
    }

    public function noReturnWithoutDocBlockMethod()
    {
    }

    public function returnMethod(): string
    {
        return '';
    }

    public function addItem($item)
    {
        $this->items[] = $item;
    }

    public function removeItem($item)
    {
        $this->items = array_values(array_diff($this->items, [$item]));
    }

    public function getItems()
    {
        return $this->items;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @return bool
     */
    public function getEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @return bool
     */
    public function canAction(): bool
    {
        return $this->action;
    }

    /**
     * @return bool
     */
    public function hasItems(): bool
    {
        return !empty($this->items);
    }

    /**
     * @return mixed
     */
    protected function getProtectedProperty()
    {
        return $this->protectedProperty;
    }

    /**
     * @param mixed $protectedProperty
     */
    protected function setProtectedProperty($protectedProperty): void
    {
        $this->protectedProperty = $protectedProperty;
    }

    protected function getAttribute($name)
    {
        return $this->attributes[$name] ?? null;
    }

    protected function setAttribute($name, $value)
    {
        $this->attributes[$name] = $value;
    }

    public function getPropertyNameAttribute()
    {
        return $this->attributes['property_name'] ?? null;
    }

    public function setPropertyNameAttribute($value): void
    {
        $this->attributes['property_name'] = $value;
    }

    /**
     * @return mixed
     */
    private function getPrivateProperty()
    {
        return $this->privateProperty;
    }

    /**
     * @param mixed $privateProperty
     */
    private function setPrivateProperty($privateProperty): void
    {
        $this->privateProperty = $privateProperty;
    }

    private function get($name)
    {
        return $this->attributes[$name] ?? null;
    }

    private function set($name, $value)
    {
        $this->attributes[$name] = $value;
    }
}
