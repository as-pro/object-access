<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Setter;

use ASPRO\ObjectAccess\SetterInterface;

class PublicStaticSetterByProperty implements SetterInterface
{
    /**
     * @var string
     */
    private $property;

    /**
     * SetterByProperty constructor.
     *
     * @param string $property
     */
    public function __construct(string $property)
    {
        $this->property = $property;
    }

    /**
     * @param object|string $object
     * @param mixed         $value
     */
    public function __invoke($object, $value): void
    {
        $object::${$this->property} = $value;
    }

    /**
     * @param \ReflectionProperty $reflection
     *
     * @return PublicStaticSetterByProperty
     */
    public static function fromReflection(\ReflectionProperty $reflection): self
    {
        return new static($reflection->getName());
    }
}
