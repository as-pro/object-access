<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Exception;

class NotFoundSetterException extends AbstractPropertyAccessException
{
    /**
     * @return string
     */
    public function genMessage(): string
    {
        return sprintf('Setter not found for %s@%s', $this->getClass(), $this->getProperty());
    }
}
