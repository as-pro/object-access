<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Finder;

use ASPRO\ObjectAccess\Getter\PublicGetterByMethod;
use ASPRO\ObjectAccess\Getter\PublicStaticGetterByMethod;
use ASPRO\ObjectAccess\Getter\ReflectionGetterByMethod;
use ASPRO\ObjectAccess\GetterFinderInterface;
use ASPRO\ObjectAccess\GetterInterface;
use ASPRO\ObjectAccess\Helper;
use ASPRO\ObjectAccess\Modifiers;

class GetterByMethodFinder implements GetterFinderInterface
{
    /**
     * @var string
     */
    private $prefix;

    /**
     * @var string
     */
    private $suffix;

    /**
     * @var Modifiers
     */
    private $modifiers;

    /**
     * GetterByMethodFinder constructor.
     *
     * @param string $prefix
     * @param string $suffix
     * @param int    $modifiers
     */
    public function __construct(string $prefix, string $suffix, int $modifiers)
    {
        $this->prefix = $prefix;
        $this->suffix = $suffix;
        $this->modifiers = new Modifiers($modifiers);
    }

    /**
     * @param string $class
     * @param string $name
     *
     * @return null|GetterInterface
     */
    public function findGetter(string $class, string $name): ?GetterInterface
    {
        $method = $this->findMethod($class, $name);
        if (null === $method) {
            return null;
        }

        if ($method->isPublic()) {
            if ($method->isStatic()) {
                return PublicStaticGetterByMethod::fromReflection($method);
            }

            return PublicGetterByMethod::fromReflection($method);
        }

        return ReflectionGetterByMethod::fromReflection($method);
    }

    /**
     * @param string $class
     * @param string $name
     *
     * @return null|\ReflectionMethod
     */
    private function findMethod(string $class, string $name): ?\ReflectionMethod
    {
        foreach ($this->possibleNames($name) as $methodName) {
            try {
                $method = new \ReflectionMethod($class, $methodName);
                if ($this->methodIsMatch($method)) {
                    return $method;
                }
            } catch (\ReflectionException $e) {
                // continue
            }
        }

        return null;
    }

    /**
     * @param string $name
     *
     * @return \Generator
     */
    private function possibleNames(string $name): \Generator
    {
        yield lcfirst(Helper::camelCase($this->prefix)).Helper::camelCase($name).Helper::camelCase($this->suffix);

        if ('' !== $this->prefix) {
            $name = $this->prefix.'_'.$name;
        }
        if ('' !== $this->suffix) {
            $name = $name.'_'.$this->suffix;
        }

        yield Helper::snakeCase($name);
    }

    /**
     * @param \ReflectionMethod $method
     *
     * @return bool
     */
    private function methodIsMatch(\ReflectionMethod $method): bool
    {
        if (0 !== $method->getNumberOfRequiredParameters()) {
            return false;
        }

        $returnType = $method->getReturnType();
        if (null !== $returnType && 'void' === $returnType->getName()) {
            return false;
        }

        return $this->modifiers->matchReflection($method);
    }
}
