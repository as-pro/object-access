<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess;

use Psr\Cache\CacheItemPoolInterface;

class CachableGetterFinder implements GetterFinderInterface
{
    /**
     * @var GetterFinderInterface
     */
    private $base;

    /**
     * @var null|CacheItemPoolInterface
     */
    private $cache;

    /**
     * @var array
     */
    private $arrayCache = [];

    /**
     * CachableGetterFinder constructor.
     *
     * @param GetterFinderInterface       $base
     * @param null|CacheItemPoolInterface $cache
     */
    public function __construct(GetterFinderInterface $base, ?CacheItemPoolInterface $cache = null)
    {
        $this->base = $base;
        $this->cache = $cache;
    }

    /**
     * @param string $class
     * @param string $name
     *
     * @throws \Psr\Cache\InvalidArgumentException
     *
     * @return null|GetterInterface
     */
    public function findGetter(string $class, string $name): ?GetterInterface
    {
        $key = __CLASS__.'..'.$class.'..'.$name;
        $key = str_replace(['\\', '/', '@', '{', '}', '(', ')', ':'], '.', $key);

        if (isset($this->arrayCache[$key])) {
            $result = $this->arrayCache[$key];

            return false === $result ? null : $result;
        }

        if (null !== $this->cache) {
            $item = $this->cache->getItem($key);
            if ($item->isHit()) {
                $result = $item->get();
                $this->arrayCache[$key] = $result;

                return false === $result ? null : $result;
            }
        }

        $result = $this->base->findGetter($class, $name);

        $this->arrayCache[$key] = $result ?? false;

        if (isset($item) && null !== $this->cache) {
            $this->cache->save($item->set($result));
        }

        return $result;
    }

    public function reset(): void
    {
        $this->arrayCache = [];
    }
}
