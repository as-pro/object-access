<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Setter;

use ASPRO\ObjectAccess\Modifiers;
use ASPRO\ObjectAccess\SetterInterface;

class ClosureSetterByProperty implements SetterInterface
{
    /**
     * @var string
     */
    private $property;

    /**
     * @var string
     */
    private $class;

    /**
     * @var Modifiers
     */
    private $modifiers;

    /**
     * @var null|\Closure
     */
    private $cb;

    /**
     * SetterByProperty constructor.
     *
     * @param string $property
     * @param string $class
     * @param int    $modifiers
     */
    public function __construct(string $property, string $class, int $modifiers)
    {
        $this->property = $property;
        $this->class = $class;
        $this->modifiers = new Modifiers($modifiers);
    }

    /**
     * @return array
     */
    public function __sleep()
    {
        return [
            'property',
            'class',
            'modifiers',
        ];
    }

    /**
     * @param object|string $object
     * @param mixed         $value
     */
    public function __invoke($object, $value): void
    {
        if (null === $this->cb) {
            $property = $this->property;
            if ($this->modifiers->isStatic()) {
                $this->cb = function ($object, $value) use ($property) {
                    $object::${$property} = $value;
                };
            } else {
                $this->cb = function (object $object, $value) use ($property) {
                    $object->{$property} = $value;
                };
            }
            if (!$this->modifiers->isPublic()) {
                $this->cb = $this->cb->bindTo(null, $this->class);
            }
        }

        $this->cb->__invoke($object, $value);
    }

    /**
     * @param \ReflectionProperty $reflection
     *
     * @return ClosureSetterByProperty
     */
    public static function fromReflection(\ReflectionProperty $reflection): self
    {
        return new static(
            $reflection->getName(),
            $reflection->getDeclaringClass()->getName(),
            Modifiers::valueFromReflection($reflection)
        );
    }
}
