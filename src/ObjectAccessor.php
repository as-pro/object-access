<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess;

use ASPRO\ObjectAccess\Exception\NotFoundGetterException;
use ASPRO\ObjectAccess\Exception\NotFoundSetterException;

class ObjectAccessor implements ObjectAccessorInterface, GetterFinderInterface, SetterFinderInterface
{
    /**
     * @var GetterFinderInterface
     */
    private $getterFinder;

    /**
     * @var SetterFinderInterface
     */
    private $setterFinder;

    /**
     * @var callable
     */
    private $classResolver;

    /**
     * @var bool
     */
    private $silence = false;

    /**
     * ObjectAccessor constructor.
     *
     * @param GetterFinderInterface $accessorFinder
     * @param SetterFinderInterface $mutatorFinder
     * @param bool                  $silence
     * @param null|callable         $classResolver
     */
    public function __construct(
        GetterFinderInterface $accessorFinder,
        SetterFinderInterface $mutatorFinder,
        bool $silence = false,
        ?callable $classResolver = null
    ) {
        $this->getterFinder = $accessorFinder;
        $this->setterFinder = $mutatorFinder;
        $this->silence = $silence;
        $this->classResolver = $classResolver ?? '\get_class';
    }

    /**
     * @param object|string $object
     *
     * @return string
     */
    public function resolveClass($object): string
    {
        if (is_string($object)) {
            return $object;
        }

        return ($this->classResolver)($object);
    }

    /**
     * @param object|string $object
     * @param string        $name
     *
     * @return null|mixed
     */
    public function getValue($object, string $name)
    {
        $class = $this->resolveClass($object);
        $getter = $this->getterFinder->findGetter($class, $name);
        if (null !== $getter) {
            return $getter->__invoke($object);
        }

        if (false === $this->silence) {
            throw new NotFoundGetterException($class, $name);
        }

        return null;
    }

    /**
     * @param string $class
     * @param string $name
     *
     * @return null|GetterInterface
     */
    public function findGetter(string $class, string $name): ?GetterInterface
    {
        return $this->getterFinder->findGetter($class, $name);
    }

    /**
     * @param object|string $object
     * @param string        $name
     * @param mixed         $value
     */
    public function setValue($object, string $name, $value): void
    {
        $class = $this->resolveClass($object);
        $setter = $this->setterFinder->findSetter($class, $name);
        if ($setter) {
            if ($setter instanceof GetterAwareInterface) {
                $accessor = $this->findGetter($class, $name);
                if ($accessor) {
                    $setter->setGetter($accessor);
                }
            }
            $setter->__invoke($object, $value);

            return;
        }

        if (!$this->silence) {
            throw new NotFoundSetterException($class, $name);
        }
    }

    /**
     * @param string $class
     * @param string $name
     *
     * @return null|SetterInterface
     */
    public function findSetter(string $class, string $name): ?SetterInterface
    {
        return $this->setterFinder->findSetter($class, $name);
    }

    /**
     * @param object|string $object
     * @param string        $name
     *
     * @return bool
     */
    public function isReadable($object, string $name): bool
    {
        $class = $this->resolveClass($object);
        $accessor = $this->findGetter($class, $name);

        return !is_null($accessor);
    }

    /**
     * @param object|string $object
     * @param string        $name
     *
     * @return bool
     */
    public function isWritable($object, string $name): bool
    {
        $class = $this->resolveClass($object);
        $mutator = $this->findSetter($class, $name);

        return !is_null($mutator);
    }
}
