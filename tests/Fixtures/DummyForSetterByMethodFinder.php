<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Tests\Fixtures;

class DummyForSetterByMethodFinder
{
    public function snake_case_property($value)
    {
    }

    public function publicProperty($value)
    {
    }

    public function setPublicProperty($value)
    {
    }

    public function setPublicPropertyMore($a, $b)
    {
    }

    public static function setStaticProperty($value)
    {
    }

    private function setPrivateProperty($value)
    {
    }
}
