<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Tests;

use ASPRO\ObjectAccess\Factory;
use ASPRO\ObjectAccess\Setter\CollectionSetter;
use ASPRO\ObjectAccess\Setter\PublicSetterByMethod;
use ASPRO\ObjectAccess\Setter\PublicSetterByProperty;
use ASPRO\ObjectAccess\Setter\PublicSetterByProxyMethod;
use ASPRO\ObjectAccess\Setter\ReflectionSetterByMethod;
use ASPRO\ObjectAccess\Setter\ReflectionSetterByProperty;
use ASPRO\ObjectAccess\SetterFinderInterface;
use ASPRO\ObjectAccess\SetterInterface;
use ASPRO\ObjectAccess\Tests\Fixtures\DummyForAccessByProperty;
use ASPRO\ObjectAccess\Tests\Fixtures\DummyForSetterByMethod;
use PHPStan\Testing\TestCase;

/**
 * @internal
 * @coversNothing
 */
class DefaultSetterFinderTest extends TestCase
{
    /**
     * @var SetterFinderInterface
     */
    protected $finder;

    protected function setUp(): void
    {
        $this->finder = Factory::newDefaultSetterFinder(true);
    }

    public function dataForTest()
    {
        yield 'public property' => [
            new class() {
                public $foo;
            },
            'foo',
            new PublicSetterByProperty('foo'),
        ];

        yield 'public property vs public set' => [
            new class() {
                public $foo;

                public function setFoo($value)
                {
                }
            },
            'foo',
            new PublicSetterByMethod('setFoo'),
        ];

        yield 'public property vs public set vs public add' => [
            new class() {
                public $items;

                public function setItems($value)
                {
                }

                public function addItem()
                {
                }
            },
            'items',
            new PublicSetterByMethod('setItems'),
        ];

        yield 'public property vs public add' => [
            new class() {
                public $items;

                public function addItem()
                {
                }
            },
            'items',
            new PublicSetterByProperty('items'),
        ];

        yield 'protected property vs public add' => [
            new class() {
                protected $items;

                public function addItem($value)
                {
                }
            },
            'items',
            new CollectionSetter(new PublicSetterByMethod('addItem')),
        ];

        yield '__set vs public property' => [
            new class() {
                public $foo;

                public function __set($name, $value)
                {
                }
            },
            'foo',
            new PublicSetterByProperty('foo'),
        ];

        yield '__set vs protected property' => [
            self::up(new class() {
                protected $foo;

                public function __set($name, $value)
                {
                }
            }, $class),
            'foo',
            new ReflectionSetterByProperty('foo', $class),
        ];

        yield '__set vs protected method' => [
            self::up(new class() {
                protected function setFoo($value)
                {
                }

                public function __set($name, $value)
                {
                }
            }, $class),
            'foo',
            new ReflectionSetterByMethod('setFoo', $class),
        ];

        yield '__set without properties' => [
            self::up(new class() {
                public function __set($name, $value)
                {
                }
            }, $class),
            'virtual',
            new PublicSetterByProxyMethod('virtual', '__set'),
        ];

        yield 'public property vs protected method' => [
            new class() {
                public $foo;

                protected function setFoo($value)
                {
                }
            },
            'foo',
            new PublicSetterByProperty('foo'),
        ];

        // protected getters

        yield 'private property' => [
            self::up(new class() {
                private $foo;

                protected function setBar($value)
                {
                }
            }, $class),
            'foo',
            new ReflectionSetterByProperty('foo', $class),
        ];

        yield 'protected property vs protected set' => [
            self::up(new class() {
                protected $foo;

                protected function setFoo($value)
                {
                }
            }, $class),
            'foo',
            new ReflectionSetterByMethod('setFoo', $class),
        ];

        yield 'private parent method' => [
            self::up(new class() extends DummyForSetterByMethod {
            }, $class),
            'private_foo',
            new ReflectionSetterByMethod('setPrivateFoo', DummyForSetterByMethod::class),
        ];

        yield 'private parent property' => [
            self::up(new class() extends DummyForAccessByProperty {
            }, $class),
            'private_foo',
            new ReflectionSetterByProperty('privateFoo', DummyForAccessByProperty::class),
        ];
    }

    public static function up(object $object, &$class)
    {
        $class = get_class($object);

        return $object;
    }

    /**
     * @dataProvider dataForTest
     * @covers \ASPRO\ObjectAccess\Factory::newDefaultSetterFinder
     *
     * @param object          $object
     * @param string          $propertyName
     * @param SetterInterface $expected
     */
    public function test(object $object, string $propertyName, SetterInterface $expected)
    {
        $setter = $this->finder->findSetter(get_class($object), $propertyName);
        $setter = unserialize(serialize($setter));
        $this->assertEquals($expected, $setter);
    }
}
