<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Tests\Fixtures;

class DummyForCollectionSetter
{
    public $items = [];

    public function addItem($item)
    {
        if (!$this->hasItem($item)) {
            $this->items[] = $item;
        }
    }

    public function hasItem($item): bool
    {
        return in_array($item, $this->items);
    }

    public function removeItem($item)
    {
        $this->items = array_values(array_diff($this->items, [$item]));
    }
}
