<?php

namespace ASPRO\ObjectAccess\Tests\Fixtures;

class DummyParent
{
    use DummyTrait;
    public $publicPropertyParent;
    protected $protectedPropertyParent;

    private $privatePropertyParent;

    /**
     * @return mixed
     */
    public function getPublicPropertyParent()
    {
        return $this->publicPropertyParent;
    }

    /**
     * @param mixed $publicPropertyParent
     */
    public function setPublicPropertyParent($publicPropertyParent): void
    {
        $this->publicPropertyParent = $publicPropertyParent;
    }

    /**
     * @return mixed
     */
    protected function getProtectedPropertyParent()
    {
        return $this->protectedPropertyParent;
    }

    /**
     * @param mixed $protectedPropertyParent
     */
    protected function setProtectedPropertyParent($protectedPropertyParent): void
    {
        $this->protectedPropertyParent = $protectedPropertyParent;
    }

    /**
     * @return mixed
     */
    private function getPrivatePropertyParent()
    {
        return $this->privatePropertyParent;
    }

    /**
     * @param mixed $privatePropertyParent
     */
    private function setPrivatePropertyParent($privatePropertyParent): void
    {
        $this->privatePropertyParent = $privatePropertyParent;
    }
}
