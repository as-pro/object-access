<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Tests\Setter;

use ASPRO\ObjectAccess\Getter\PublicGetterByProperty;
use ASPRO\ObjectAccess\Setter\CollectionSetter;
use ASPRO\ObjectAccess\Setter\PublicSetterByMethod;
use ASPRO\ObjectAccess\Tests\Fixtures\DummyForCollectionSetter;
use PHPUnit\Framework\TestCase;

/**
 * @covers \ASPRO\ObjectAccess\Setter\CollectionSetter
 *
 * @internal
 */
class CollectionSetterTest extends TestCase
{
    /**
     * @var DummyForCollectionSetter
     */
    protected $dummy;

    protected function setUp(): void
    {
        $this->dummy = new DummyForCollectionSetter();
    }

    public function testOnlyAdder()
    {
        $setter = new CollectionSetter(
            new PublicSetterByMethod('addItem')
        );

        $this->dummy->items = ['a', 'b'];

        $setter->__invoke($this->dummy, null);
        $this->assertEquals(
            ['a', 'b'],
            $this->dummy->items
        );

        $setter->__invoke($this->dummy, 'c');
        $this->assertEquals(
            ['a', 'b', 'c'],
            $this->dummy->items
        );

        $setter->__invoke($this->dummy, ['d', 'e']);
        $this->assertEquals(
            ['a', 'b', 'c', 'd', 'e'],
            $this->dummy->items
        );

        $setter->__invoke($this->dummy, new \IteratorIterator(new \ArrayObject(['f', 'g'])));
        $this->assertEquals(
            ['a', 'b', 'c', 'd', 'e', 'f', 'g'],
            $this->dummy->items
        );
    }

    public function testAdderWithGetter()
    {
        $setter = new CollectionSetter(
            new PublicSetterByMethod('addItem')
        );
        $setter->setGetter(new PublicGetterByProperty('items'));

        $this->dummy->items = ['a', 'b'];

        $setter->__invoke($this->dummy, ['b', 'c']);
        $this->assertEquals(
            ['a', 'b', 'c'],
            $this->dummy->items
        );
    }

    public function testAdderWithGetterAndRemover()
    {
        $setter = new CollectionSetter(
            new PublicSetterByMethod('addItem'),
            new PublicSetterByMethod('removeItem'),
            new PublicGetterByProperty('items')
        );

        $this->dummy->items = ['a', 'b'];

        $setter->__invoke($this->dummy, ['b', 'c']);

        $this->assertEquals(
            ['b', 'c'],
            $this->dummy->items
        );
    }
}
