<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess;

interface GetterAwareInterface
{
    /**
     * @param GetterInterface $accessor
     */
    public function setGetter(GetterInterface $accessor): void;
}
