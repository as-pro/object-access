<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Getter;

use ASPRO\ObjectAccess\GetterInterface;
use ASPRO\ObjectAccess\Modifiers;

class ClosureGetterByMethod implements GetterInterface
{
    /**
     * @var string
     */
    private $method;

    /**
     * @var string
     */
    private $class;

    /**
     * @var Modifiers
     */
    private $modifiers;

    /**
     * @var null|\Closure
     */
    private $cb;

    /**
     * GetterByMethod constructor.
     *
     * @param string $method
     * @param string $class
     * @param int    $modifiers
     */
    public function __construct(
        string $method,
        string $class,
        int $modifiers
    ) {
        $this->method = $method;
        $this->class = $class;
        $this->modifiers = new Modifiers($modifiers);
    }

    /**
     * @return array
     */
    public function __sleep()
    {
        return [
            'method',
            'class',
            'modifiers',
        ];
    }

    /**
     * @param object|string $object
     *
     * @return mixed
     */
    public function __invoke($object)
    {
        if (null === $this->cb) {
            $method = $this->method;
            if ($this->modifiers->isStatic()) {
                $this->cb = function ($object) use ($method) {
                    return $object::{$method}();
                };
            } else {
                $this->cb = function (object $object) use ($method) {
                    return $object->{$method}();
                };
            }
            if (!$this->modifiers->isPublic()) {
                $this->cb = $this->cb->bindTo(null, $this->class);
            }
        }

        return $this->cb->__invoke($object);
    }

    /**
     * @param \ReflectionMethod $reflection
     *
     * @return ClosureGetterByMethod
     */
    public static function fromReflection(\ReflectionMethod $reflection): self
    {
        return new static(
            $reflection->getName(),
            $reflection->getDeclaringClass()->getName(),
            Modifiers::valueFromReflection($reflection)
        );
    }
}
