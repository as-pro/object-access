<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Tests\Setter;

use ASPRO\ObjectAccess\Modifiers;
use ASPRO\ObjectAccess\Setter\ClosureSetterByProxyMethod;
use ASPRO\ObjectAccess\Setter\PublicSetterByProxyMethod;
use ASPRO\ObjectAccess\Setter\PublicStaticSetterByProxyMethod;
use ASPRO\ObjectAccess\Setter\ReflectionSetterByProxyMethod;
use ASPRO\ObjectAccess\Tests\Fixtures\DummyForSetterByProxyMethod;
use PHPStan\Testing\TestCase;

/**
 * @internal
 * @coversNothing
 */
class SetterByProxyMethodTest extends TestCase
{
    /**
     * @var DummyForSetterByProxyMethod
     */
    protected $dummy;
    protected $propertyName = 'test_property';

    protected function setUp(): void
    {
        $this->dummy = new DummyForSetterByProxyMethod();
    }

    public function dataProxyMethods(): array
    {
        return [
            ['set'],
            ['setProtected'],
            ['setPrivate'],
            ['setStatic'],
            ['setProtectedStatic'],
            ['setPrivateStatic'],
        ];
    }

    /**
     * @covers \ASPRO\ObjectAccess\Setter\PublicSetterByProxyMethod
     * @dataProvider dataProxyMethods
     *
     * @param string $methodName
     */
    public function testPublicSetter(string $methodName)
    {
        /** @var Modifiers $modifiers */
        list($reflection, $getter, $modifiers) = $this->getInfo($methodName);

        $setter = PublicSetterByProxyMethod::fromReflection($reflection, $this->propertyName);
        $setter = unserialize(serialize($setter));

        $this->assertEquals(
            new PublicSetterByProxyMethod($this->propertyName, $methodName),
            $setter
        );

        $value = 'test_value';

        if ($modifiers->isPublic()) {
            $setter($this->dummy, $value);
            $this->assertEquals(
                $getter(),
                $value
            );
        } else {
            $exception = null;

            try {
                $setter($this->dummy, $value);
            } catch (\Throwable $e) {
                $exception = $e;
            }
            $this->assertNotNull($exception);
        }
    }

    /**
     * @covers \ASPRO\ObjectAccess\Setter\PublicStaticSetterByProxyMethod
     * @dataProvider dataProxyMethods
     *
     * @param string $methodName
     */
    public function testPublicStaticSetter(string $methodName)
    {
        /** @var Modifiers $modifiers */
        list($reflection, $getter, $modifiers) = $this->getInfo($methodName);

        $setter = PublicStaticSetterByProxyMethod::fromReflection($reflection, $this->propertyName);
        $setter = unserialize(serialize($setter));

        $class = get_class($this->dummy);

        $this->assertEquals(
            new PublicStaticSetterByProxyMethod($this->propertyName, $methodName),
            $setter
        );

        $value = 'test_value';

        if ($modifiers->isPublic() && $modifiers->isStatic()) {
            $setter($class, $value);
            $this->assertEquals(
                $getter(),
                $value
            );

            $value = 'static_value';
            $setter($this->dummy, $value);
            $this->assertEquals(
                $getter(),
                $value
            );
        } else {
            $exception = null;

            try {
                $setter($this->dummy, $value);
            } catch (\Throwable $e) {
                $exception = $e;
            }
            $this->assertNotNull($exception);
        }
    }

    /**
     * @covers \ASPRO\ObjectAccess\Setter\ClosureSetterByProxyMethod
     * @dataProvider dataProxyMethods
     *
     * @param string $methodName
     */
    public function testClosureSetter(string $methodName)
    {
        /** @var Modifiers $modifiers */
        list($reflection, $getter, $modifiers) = $this->getInfo($methodName);

        $setter = ClosureSetterByProxyMethod::fromReflection($reflection, $this->propertyName);
        $setter = unserialize(serialize($setter));

        $class = get_class($this->dummy);

        $this->assertEquals(
            new ClosureSetterByProxyMethod(
                $this->propertyName,
                $methodName,
                $class,
                $modifiers->getValue()
            ),
            $setter
        );

        $value = 'test_value';
        $setter($this->dummy, $value);
        $this->assertEquals($getter(), $value);

        if ($modifiers->isStatic()) {
            $value = 'static_test';
            $setter($class, $value);
            $this->assertEquals($getter(), $value);
        } else {
            $this->expectException(\Throwable::class);
            $setter($class, $value);
        }
    }

    /**
     * @covers \ASPRO\ObjectAccess\Setter\ReflectionSetterByProxyMethod
     * @dataProvider dataProxyMethods
     *
     * @param string $methodName
     */
    public function testReflectionSetter(string $methodName)
    {
        /** @var Modifiers $modifiers */
        list($reflection, $getter, $modifiers) = $this->getInfo($methodName);

        $setter = ReflectionSetterByProxyMethod::fromReflection($reflection, $this->propertyName);
        $setter = unserialize(serialize($setter));

        $class = get_class($this->dummy);

        $this->assertEquals(
            new ReflectionSetterByProxyMethod(
                $this->propertyName,
                $methodName,
                $class
            ),
            $setter
        );

        $value = 'test_value';
        $setter($this->dummy, $value);
        $this->assertEquals($getter(), $value);

        if ($modifiers->isStatic()) {
            $value = 'static_test';
            $setter($class, $value);
            $this->assertEquals($getter(), $value);
        } else {
            $this->expectException(\Throwable::class);
            $setter($class, $value);
        }
    }

    protected function getInfo(string $methodName): array
    {
        $reflection = new \ReflectionMethod($this->dummy, $methodName);
        $reflection->setAccessible(true);

        if ($reflection->isStatic()) {
            $getter = function () {
                return DummyForSetterByProxyMethod::$staticProperties[$this->propertyName] ?? null;
            };
        } else {
            $getter = function () {
                return $this->dummy->properties[$this->propertyName] ?? null;
            };
        }

        return [$reflection, $getter, Modifiers::newFromReflection($reflection)];
    }
}
