<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Tests\Finder;

use ASPRO\ObjectAccess\Finder\SetterByPropertyFinder;
use ASPRO\ObjectAccess\Modifiers;
use ASPRO\ObjectAccess\Setter\PublicSetterByProperty;
use ASPRO\ObjectAccess\Setter\PublicStaticSetterByProperty;
use ASPRO\ObjectAccess\Setter\ReflectionSetterByProperty;
use ASPRO\ObjectAccess\Tests\Fixtures\DummyForAccessByProperty;
use PHPUnit\Framework\TestCase;

/**
 * @covers \ASPRO\ObjectAccess\Finder\SetterByPropertyFinder
 *
 * @internal
 */
class SetterByPropertyFinderTest extends TestCase
{
    public function test()
    {
        $finder = new SetterByPropertyFinder(Modifiers::IS_PUBLIC);
        $setter = $finder->findSetter(DummyForAccessByProperty::class, 'foo');
        $this->assertEquals(
            new PublicSetterByProperty('foo'),
            $setter
        );
    }

    public function testNotFound()
    {
        $finder = new SetterByPropertyFinder(Modifiers::IS_PUBLIC);
        $setter = $finder->findSetter(DummyForAccessByProperty::class, 'not_found');
        $this->assertNull($setter);
    }

    public function testStatic()
    {
        $finder = new SetterByPropertyFinder(Modifiers::IS_PUBLIC | Modifiers::IS_STATIC);
        $setter = $finder->findSetter(DummyForAccessByProperty::class, 'static_foo');
        $this->assertEquals(
            new PublicStaticSetterByProperty('staticFoo'),
            $setter
        );
    }

    public function testPrivate()
    {
        $finder = new SetterByPropertyFinder(Modifiers::IS_PRIVATE);
        $setter = $finder->findSetter(DummyForAccessByProperty::class, 'private_foo');
        $setter = unserialize(serialize($setter));
        $this->assertEquals(
            new ReflectionSetterByProperty('privateFoo', DummyForAccessByProperty::class),
            $setter
        );
    }

    public function testPriority()
    {
        $finder = new SetterByPropertyFinder(Modifiers::IS_PUBLIC);
        $setter = $finder->findSetter(DummyForAccessByProperty::class, 'native_name');
        $this->assertEquals(
            new PublicSetterByProperty('native_name'),
            $setter
        );
    }

    public function testCase()
    {
        $finder = new SetterByPropertyFinder(Modifiers::IS_PUBLIC);
        $setter = $finder->findSetter(DummyForAccessByProperty::class, 'NaTiVeNaMe');
        $this->assertNull($setter);
    }
}
