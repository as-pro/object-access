<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Finder;

use ASPRO\ObjectAccess\Helper;
use ASPRO\ObjectAccess\Modifiers;
use ASPRO\ObjectAccess\Setter\PublicSetterByMethod;
use ASPRO\ObjectAccess\Setter\PublicStaticSetterByMethod;
use ASPRO\ObjectAccess\Setter\ReflectionSetterByMethod;
use ASPRO\ObjectAccess\SetterFinderInterface;
use ASPRO\ObjectAccess\SetterInterface;

class SetterByMethodFinder implements SetterFinderInterface
{
    /**
     * @var string
     */
    private $prefix;

    /**
     * @var string
     */
    private $suffix;

    /**
     * @var Modifiers
     */
    private $modifiers;

    /**
     * GetterByMethodFinder constructor.
     *
     * @param string $prefix
     * @param string $suffix
     * @param int    $modifiers
     */
    public function __construct(string $prefix, string $suffix, int $modifiers)
    {
        $this->prefix = $prefix;
        $this->suffix = $suffix;
        $this->modifiers = new Modifiers($modifiers);
    }

    /**
     * @param string $class
     * @param string $name
     *
     * @return null|SetterInterface
     */
    public function findSetter(string $class, string $name): ?SetterInterface
    {
        $method = $this->findMethod($class, $name);
        if (null === $method) {
            return null;
        }

        if ($method->isPublic()) {
            if ($method->isStatic()) {
                return PublicStaticSetterByMethod::fromReflection($method);
            }

            return PublicSetterByMethod::fromReflection($method);
        }

        return ReflectionSetterByMethod::fromReflection($method);
    }

    /**
     * @param string $class
     * @param string $name
     *
     * @return null|\ReflectionMethod
     */
    private function findMethod(string $class, string $name): ?\ReflectionMethod
    {
        foreach ($this->possibleNames($name) as $methodName) {
            try {
                $method = new \ReflectionMethod($class, $methodName);
                if ($this->methodIsMatch($method)) {
                    return $method;
                }
            } catch (\ReflectionException $e) {
                // continue
            }
        }

        return null;
    }

    /**
     * @param string $name
     *
     * @return \Generator
     */
    private function possibleNames(string $name): \Generator
    {
        yield lcfirst(Helper::camelCase($this->prefix)).Helper::camelCase($name).Helper::camelCase($this->suffix);

        if ('' !== $this->prefix) {
            $name = $this->prefix.'_'.$name;
        }
        if ('' !== $this->suffix) {
            $name = $name.'_'.$this->suffix;
        }

        yield Helper::snakeCase($name);
    }

    /**
     * @param \ReflectionMethod $method
     *
     * @return bool
     */
    private function methodIsMatch(\ReflectionMethod $method): bool
    {
        if (0 === $method->getNumberOfParameters()) {
            return false;
        }

        if ($method->getNumberOfRequiredParameters() > 1) {
            return false;
        }

        return $this->modifiers->matchReflection($method);
    }
}
