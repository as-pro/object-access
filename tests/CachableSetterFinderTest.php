<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Tests;

use ASPRO\ObjectAccess\CachableSetterFinder;
use ASPRO\ObjectAccess\Setter\PublicSetterByMethod;
use ASPRO\ObjectAccess\SetterFinderInterface;
use PHPStan\Testing\TestCase;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

/**
 * @internal
 * @covers \ASPRO\ObjectAccess\CachableSetterFinder
 */
class CachableSetterFinderTest extends TestCase
{
    public function test()
    {
        /** @var MockObject|SetterFinderInterface $finder */
        $finder = $this->getMockBuilder(SetterFinderInterface::class)
            ->setMethods(['findSetter'])
            ->getMock()
            ;

        $args = ['class', 'property'];
        $getter = new PublicSetterByMethod('test');

        $finder->expects($this->once())
            ->method('findSetter')
            ->with(...$args)
            ->willReturn($getter)
            ;

        $cache = new FilesystemAdapter();
        $cache->clear();

        $cachable = new CachableSetterFinder($finder, $cache);

        $result = $cachable->findSetter(...$args);
        $this->assertEquals($getter, $result);

        $result = $cachable->findSetter(...$args);
        $this->assertEquals($getter, $result);

        $cachable->reset();
        $result = $cachable->findSetter(...$args);
        $this->assertEquals($getter, $result);

        $cache->clear();
    }
}
