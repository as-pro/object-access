<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess;

class ComplexSetterFinder implements SetterFinderInterface
{
    /**
     * @var iterable|SetterFinderInterface[]
     */
    private $factories;

    /**
     * AccessorFactory constructor.
     *
     * @param iterable|SetterFinderInterface[] $factories
     */
    public function __construct(iterable $factories)
    {
        $this->factories = $factories;
    }

    /**
     * @param string $class
     * @param string $name
     *
     * @return null|SetterInterface
     */
    public function findSetter(string $class, string $name): ?SetterInterface
    {
        foreach ($this->factories as $factory) {
            $mutator = $factory->findSetter($class, $name);
            if (!is_null($mutator)) {
                return $mutator;
            }
        }

        return null;
    }
}
