<?php

declare(strict_types=1);

namespace ASPRO\ObjectAccess\Tests\Finder;

use ASPRO\ObjectAccess\Finder\GetterByProxyMethodFinder;
use ASPRO\ObjectAccess\Finder\SetterByProxyMethodFinder;
use ASPRO\ObjectAccess\Modifiers;
use ASPRO\ObjectAccess\Setter\PublicSetterByProxyMethod;
use ASPRO\ObjectAccess\Setter\PublicStaticSetterByProxyMethod;
use ASPRO\ObjectAccess\Setter\ReflectionSetterByProxyMethod;
use ASPRO\ObjectAccess\Tests\Fixtures\DummyForSetterByProxyMethodFinder;
use PHPUnit\Framework\TestCase;

/**
 * @covers \ASPRO\ObjectAccess\Finder\SetterByProxyMethodFinder
 *
 * @internal
 */
class SetterByProxyMethodFinderTest extends TestCase
{
    public function test()
    {
        $finder = new SetterByProxyMethodFinder('set', Modifiers::IS_PUBLIC);
        $setter = $finder->findSetter(DummyForSetterByProxyMethodFinder::class, 'virtual');
        $this->assertEquals(
            new PublicSetterByProxyMethod('virtual', 'set'),
            $setter
        );
    }

    public function testStatic()
    {
        $finder = new SetterByProxyMethodFinder('setStatic', Modifiers::IS_PUBLIC | Modifiers::IS_STATIC);
        $setter = $finder->findSetter(DummyForSetterByProxyMethodFinder::class, 'virtual');
        $this->assertEquals(
            new PublicStaticSetterByProxyMethod('virtual', 'setStatic'),
            $setter
        );
    }

    public function testPrivate()
    {
        $finder = new SetterByProxyMethodFinder('setPrivate', Modifiers::IS_PRIVATE);
        $setter = $finder->findSetter(DummyForSetterByProxyMethodFinder::class, 'virtual');
        $setter = unserialize(serialize($setter));
        $this->assertEquals(
            new ReflectionSetterByProxyMethod('virtual', 'setPrivate', DummyForSetterByProxyMethodFinder::class),
            $setter
        );
    }

    public function testNotFound()
    {
        $finder = new SetterByProxyMethodFinder('setAttribute', Modifiers::IS_PUBLIC);
        $setter = $finder->findSetter(DummyForSetterByProxyMethodFinder::class, 'virtual');
        $this->assertNull($setter);
    }

    public function testParameters()
    {
        $this->assertTrue(method_exists(DummyForSetterByProxyMethodFinder::class, 'setMoreParams'));
        $finder = new SetterByProxyMethodFinder('setMoreParams', Modifiers::IS_PUBLIC);
        $this->assertNotNull($finder->findSetter(DummyForSetterByProxyMethodFinder::class, 'virtual'));

        $this->assertTrue(method_exists(DummyForSetterByProxyMethodFinder::class, 'setLessParams'));
        $finder = new SetterByProxyMethodFinder('setLessParams', Modifiers::IS_PUBLIC);
        $this->assertNull($finder->findSetter(DummyForSetterByProxyMethodFinder::class, 'virtual'));

        $this->assertTrue(method_exists(DummyForSetterByProxyMethodFinder::class, 'setMoreReqParams'));
        $finder = new SetterByProxyMethodFinder('setMoreReqParams', Modifiers::IS_PUBLIC);
        $this->assertNull($finder->findSetter(DummyForSetterByProxyMethodFinder::class, 'virtual'));
    }

    public function testVoid()
    {
        $finder = new SetterByProxyMethodFinder('setVoid', Modifiers::IS_PUBLIC);
        $setter = $finder->findSetter(DummyForSetterByProxyMethodFinder::class, 'virtual');
        $this->assertNull($setter);
    }
}
